#!/usr/bin/env python
#-*- coding: utf-8 -*-

from collorg.controller.controller import Controller

ctrl = Controller()
db = ctrl.db
table = db.table

topic = table('collorg.web.topic')
groups = table('collorg.group.group')
for group in groups:
    topic.title_.value = group.name_.value
    if topic.count() == 1:
        try:
            print("Removing topic {}".format(topic.title_.value))
            db.transaction_begin()
            ga = topic._rev_group_access_accessed_data_
            ga.delete()
            gb = topic._rev_group_access_group_data_
            gb.delete()
            topic._rev_access_.delete()
            topic.delete()
            db.commit()
        except Exception as err:
            print(err)
