#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys
from collorg.controller.controller import Controller

db = Controller().db
relation = db.relation

topic = relation('collorg.web.topic', cog_oid_=sys.argv[1])

output = """
Title : {title}
Action : {action}
"""

topic.get()
output_dict = {
	'title':topic.title_.value,
	'action':None
}

if topic.action_.value:
	action = db.get_elt_by_oid(topic.action_.value)
	output_dict['action'] = "{data_type_}.{name_}()".format(
		**action.__dict__)

print(output.format(**output_dict))
