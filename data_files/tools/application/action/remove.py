#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys
from collorg.controller.controller import Controller

ctrl = Controller()
db = ctrl.db
table = db.table

dt = table('collorg.core.data_type', fqtn_=sys.argv[2]).get()
action = table('collorg.application.action')
action.name_.value = sys.argv[1]
action.data_type_.value = sys.argv[2]

action.get()
a_action_task = action._rev_a_action_task_
a_action_task.delete()
action.delete()
