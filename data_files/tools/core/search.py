#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from collorg.controller.controller import Controller

"""
get all elts referencing sys.argv[1] (cog_oid)
"""

if __name__ == '__main__':
    db = Controller().db
    value = sys.argv[1]
    for relation, count in db.search(value):
        names = []
        for elt in relation:
            for field in elt._cog_fields:
                if field.value == sys.argv[1]:
                    names.append(field.name)
        print("{} : {} {}".format(relation.fqtn, count, ", ".join(names)))
        if relation.fqtn.split('.')[-1] == 'oid_table':
            print(relation._cog_json_dumps())
