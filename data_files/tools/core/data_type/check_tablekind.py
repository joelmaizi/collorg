#!/usr/bin/env python
#-*- coding: utf-8 -*-

from collorg.controller.controller import Controller

ctrl = Controller()
db = ctrl.db
relation = db.relation

relation('collorg.core.data_type').check_tablekind()
