#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys
from collorg.controller.controller import Controller

if sys.version_info.major < 3:
    input = raw_input


def menage(data_types):
    removed = []
    redo = []
    for dt in data_types:
        try:
            table(dt.fqtn_.value)
        except Exception as err:
            print("{}: {}".format(dt.fqtn_, err))
            ok = input('Remove entry from database [N/y] ? ')
            if ok.upper() == 'Y':
                try:
                    dt._rev_field_.delete()
                    dt.delete()
                    removed.append(db._fqtn_2_sql_fqtn(dt.fqtn_.value))
                except:
                    redo.append(dt)
    for dt in removed:
        db.raw_sql('drop table {}'.format(dt), {})
    if redo:
        menage(redo)

if __name__ == '__main__':
    db = Controller().db
    table = db.table
    data_types = table('collorg.core.data_type')
    menage(data_types)

