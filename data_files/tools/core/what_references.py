#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from collorg.controller.controller import Controller

"""
get all elts referencing sys.argv[1] (cog_oid)
"""

if __name__ == '__main__':
    db = Controller().db
    ref = sys.argv[1]
    for relation, count in db.what_references(ref):
        print("{}: {}".format(relation.fqtn, count))
