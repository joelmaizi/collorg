#!/usr/bin/env python
#-*- coding: utf-8 -*-

from collorg.controller.controller import Controller

ctrl = Controller()
db = ctrl.db
table = db.table

ctrl_k = Controller('collorg_db')
db_k = ctrl_k.db
table_k = db_k.table

for ns in table('collorg.core.namespace'):
    dt = ns._rev_data_type_
    if dt.count() == 0:
        print("Removing {}.".format(ns.name_))
        ns.delete()

for field in table('collorg.core.field'):
    if field.fqfn_.value.find('collorg.') != 0:
        continue
    field_k = table_k('collorg.core.field')
    field_k.fqfn_.value = field.fqfn_.value
    if field_k.count() == 0:
        print("Removing {}.".format(field.fqfn_))
        field.delete()
