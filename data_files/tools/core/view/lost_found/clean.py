#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys
from collorg.controller.controller import Controller

"""
Cleans the lost+found tuples
"""

Controller().db.relation('collorg.core.view.lost_found').clean()
