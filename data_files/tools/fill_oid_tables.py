#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys
import inspect
from collorg.controller.controller import Controller

ctrl = Controller()
model = ctrl.db
relation = model.relation

tables = relation('collorg.core.data_type')

for table in tables:
    fqtn = table.fqtn_.value
    print(fqtn)
    if fqtn.find('collorg.core.') == 0 and fqtn.count('.') == 2:
        continue
    rel = relation(fqtn)
    if rel._cog_tablename == 'oid_table':
        continue
    if not 'cog_oid_' in rel.__dict__ or not 'cog_fqtn_' in rel.__dict__:
        continue
    if rel._cog_schemaname.split('.')[-1] == 'view':
        continue
    namespaces = []
    for cls in inspect.getmro(rel.__class__):
        namespace = None
        if '_cog_schemaname' in cls.__dict__:
            namespace = cls._cog_schemaname
        if not namespace or namespace in namespaces:
            continue
        namespaces.append(namespace)
    print("{}: {}".format(rel.fqtn, namespaces))
    req = []
    rel.cog_fqtn_.value = fqtn
    for elt in rel:
        for namespace in namespaces:
            if namespace == 'collorg.core':
                continue
            req.append("""INSERT INTO "{}".oid_table VALUES """
                """('{}', '{}')""".format(namespace, elt.cog_oid_, elt.cog_fqtn_))
    try:
        sql = ";\n".join(req)
        model.commit(sql=sql)
        print('OK')
    except:
        pass
