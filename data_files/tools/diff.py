#!/usr/bin/env python
#-*- coding: utf-8 -*-

from collorg.controller.controller import Controller

core_ctrl = Controller('collorg_db')
core_db = core_ctrl.db
core_relation = core_db.table

ctrl = Controller()
db = ctrl.db
relation = db.table

schemas = []
for data_type in relation('collorg.core.data_type'):
    fqtn = data_type.fqtn_.value
    schemas.append(fqtn.rsplit('.', 1)[0])
    if fqtn.find('collorg.') != 0:
        continue
    try:
        core_relation(fqtn)
    except:
        print('{} not in core?'.format(fqtn))
        remove = raw_input('remove [y/N]? ')
        if remove.upper() == 'Y':
            sql_fqtn = ".".join(
                ['"{}"'.format(elt) for elt in fqtn.rsplit('.', 1)])
            field = relation('collorg.core.field')
            field.fqfn_.value = '{}%'.format(fqtn), 'like'
            field.delete()
            data_type = relation('collorg.core.data_type')
            data_type.fqtn_.value = fqtn
            data_type.delete()
            try:
                db.commit('drop table {} cascade'.format(sql_fqtn))
            except:
                db.commit('drop view {}'.format(sql_fqtn))

# we remove empty schemas
for schema in schemas:
    try:
        db.commit('drop schema "{}"'.format(schema))
        print('dropping schema "{}"'.format(schema))
    except:
        pass

for data_type in core_relation('collorg.core.data_type'):
    fqtn = data_type.fqtn_.value
    try:
        relation(fqtn)
    except:
        print('{} missing in application?'.format(fqtn))
