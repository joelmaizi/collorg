#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys
import os
from collorg.controller.controller import Controller
from collorg.utils.mail import Mail

"""
Add users from file.
A line must be in the format :
<first Name>,<last Name>,<email>,<pseudo>
"""

if __name__ == '__main__':
    ctrl = Controller()

    db = ctrl.db
    url = "{}://{}".format(db._cog_params['url_scheme'], db._cog_params['url'])
    script_path = '/'.join(sys.argv[0].split('/')[:-1])
    sender_pseudo = sys.argv[2]
    sender = db.table('collorg.actor.user', pseudo_ = sender_pseudo).get()
    d_mesg = eval(open("{}/reset_passwd_msg_fr.txt".format(script_path)).read())
    title = d_mesg['title']
    message = d_mesg['message']
    email = sys.argv[1]
    user = db.table('collorg.actor.user', email_=email).get()
    ok = raw_input('Change password for {} {} [N/y]? '.format(
    user.first_name_, user.last_name_
    ))
    if ok.upper() != 'Y':
        sys.exit()
    try:
        passwd = os.popen('makepasswd 2> /dev/null').read().strip()
        if not passwd:
            sys.stderr.write(
                "The makepasswd command is missing. Please install it!\n")
            sys.exit(1)
        user.force_update_passwd(new_password=passwd)
        mail = Mail(db)
        mail.set_from(sender.email_.value)
        mail.set_to([email])
        mail.set_subject(title.format(url))
        mail.set_body(message.format(
            url, passwd, sender.first_name_, sender.last_name_))
        mail.send()
    except Exception as err:
        print(err)
