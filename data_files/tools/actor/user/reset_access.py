#!/usr/bin/env python
#-*- coding: utf-8 -*-

"""
removes every access with "user" = data.
adds for each user and access on itself.
"""

from collorg.controller.controller import Controller

ctrl = Controller()
db = ctrl.db
table = db.table

access = table('collorg.access.access')
users = table('collorg.actor.user')

access.data_.value = users.cog_oid_

print("removing all access user->user")
access.delete()

for user in users:
    print("adding access {}->{}".format(user.pseudo_, user.pseudo_))
    user.grant_access(user, write=True, manage=True)
