#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys
from collorg.controller.controller import Controller

table = Controller().db.table

users = table('collorg.actor.user')
users.last_name_.value = "%{}%".format(sys.argv[1]), 'ilike'
users.order_by(users.last_name_, users.first_name_)
for user in users:
	print("{} {} : {}".format(user.last_name_, user.first_name_, user.pseudo_))
