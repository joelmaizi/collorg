#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys
from collorg.controller.controller import Controller

ctrl = Controller()
model = ctrl.db
relation = model.relation

post_oid = sys.argv[1]
post = model.get_elt_by_oid(post_oid)
allowed_post = relation('collorg.communication.allowed_post_type')
post_type = sys.argv[2]
pt = relation(post_type)
assert pt.is_cog_post
dt = relation('collorg.core.data_type')
dt.fqtn_.value = post_type
allowed_post._post_ = post
allowed_post._allowed_type_ = dt
if allowed_post.is_empty():
    allowed_post.insert()

