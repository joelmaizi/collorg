#!/usr/bin/env python
#-*- coding: utf-8 -*-

from collorg.controller.controller import Controller

ctrl = Controller()
db = ctrl.db
table = db.table

if __name__ == '__main__':
    acronym = raw_input('Acronym: ')
    name = raw_input('Name: ')
    uo = table('collorg.organization.unit')
    uo.acronym_.value = acronym
    uo.name_.value = name
    if uo.count() == 0:
        uo.insert()
