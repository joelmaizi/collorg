#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys
from collorg.controller.controller import Controller

if __name__ == '__main__':
    ctrl = Controller()
    db = ctrl.db
    table = db.table

    fqtn = sys.argv[1]
    data = table(fqtn)
    data.cog_fqtn_.value = fqtn
    access = data._rev_access_
    access.write_.value = True
    print(", ".join([user.email_.value for user in access._user_]))
