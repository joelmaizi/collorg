#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys
import argparse
from uuid import UUID
from collorg.controller.controller import Controller

def grant_access(user, accessed_data, write, manage):
    user_access = user.db.relation('collorg.access.access')
    user_access._data_ = accessed_data
    user_access._user_ = user
    if write:
        user_access.write_.value = write
        user_access.manage_.value = manage
        user_access.insert()
        print("Access granted")
    else:
        print("Access already granted")

if __name__ == '__main__':
    db = Controller().db
    relation = db.relation
    parser = argparse.ArgumentParser()
    parser.add_argument("user", help="user's pseudo")
    parser.add_argument("data", help="data OID", type=UUID)
    parser.add_argument(
        "-w", "--write", help="grant write access", action="store_true")
    parser.add_argument(
        "-m", "--manage", help="grant manager access", action="store_true")
    args = parser.parse_args()
    try:
        user = relation("collorg.actor.user", pseudo_=args.user).get()
    except:
        parser.print_help()
        print("\nuser {} does not exit!".format(args.user))
        exit(1)
    try:
        data = db.get_elt_by_oid(args.data)
    except:
        parser.print_help()
        print("\ndata with oid {} not found!".format(args.data))
        exit(1)
    manage = args.manage
    write = args.write
    with_ = []
    args.write and with_.append("write")
    args.manage and with_.append("manage")
    plural = len(with_) > 1 and "s" or ""
    print("Grant access for {} on {}{}".format(
        user.pseudo_, data.title_,
        with_ and " with {} right{}".format(" and ".join(with_), plural) or ""
    ))
    go = raw_input("Proceed [y/N]? ")
    if go.strip().upper() == "Y":
        grant_access(user, data, write, manage)
