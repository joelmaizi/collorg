#!/usr/bin/env python
#-*- coding: utf-8 -*-

from collorg.controller.controller import Controller

if __name__ == '__main__':
    ctrl = Controller()
    db = ctrl.db
    table = db.table

    functions = table('collorg.actor.function')
    functions.order_by(functions.long_name_)
    for function in functions:
        print(function.long_name_)
    function_name = raw_input("Role name: ").strip()
    if function_name:
        function = functions()
        function.long_name_.value = function_name
        for user in function._rev_role_._access_._user_:
            print(user.email_)
