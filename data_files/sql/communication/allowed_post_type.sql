CREATE TABLE "collorg.communication".allowed_post_type (
   cog_oid C_OID UNIQUE NOT NULL,
   FOREIGN KEY(cog_oid) REFERENCES "collorg.core".oid_table(cog_oid)
      INITIALLY DEFERRED,
   cog_fqtn C_FQTN
      DEFAULT 'collorg.communication.allowed_post_type'
      CHECK( cog_fqtn = 'collorg.communication.allowed_post_type' ),
   allowed_type text NOT NULL,
   FOREIGN KEY (allowed_type) REFERENCES "collorg.core".data_type(fqtn),
   post C_OID NOT NULL,
   FOREIGN KEY (post) REFERENCES "collorg.communication".oid_table(cog_oid),
   PRIMARY KEY(allowed_type, post)
)INHERITS("collorg.core".base_table);
