CREATE TABLE "collorg.communication.blog".a_post_data (
   post c_oid,
   FOREIGN KEY(post) REFERENCES "collorg.communication".oid_table(cog_oid),
--   FOREIGN KEY(post) REFERENCES "collorg.communication.blog".oid_post(cog_oid),
   data c_oid,
   FOREIGN KEY(data) REFERENCES "collorg.communication".oid_table(cog_oid),
   who c_oid,
   FOREIGN KEY(who) REFERENCES "collorg.actor"."user"(cog_oid),
   "when" timestamp(0) default ('now'::text)::timestamp(0) with time zone,
   private_reference boolean default 'f',
   "order" int default 0,
   see_also boolean default 'f',
   PRIMARY KEY(post, data)
);
