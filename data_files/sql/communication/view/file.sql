set search_path = "collorg.communication";
create view "collorg.communication.view".file as
  select
  file.cog_oid,
  file.cog_creat_date,
  file.name,
  file.signature,
  file.visibility,
  base_table.cog_oid as data_oid,
  base_table.cog_fqtn as data_fqtn,
  base_table.cog_creat_date as data_creat_date,
  base_table.title as data_title,
  base_table.introductory_paragraph as data_introductory_paragraph,
  base_table.text as data_text,
  base_table.author as data_author_oid
  from
  file
  join attachment on
  file.cog_oid = attachment.ref
  join base_table on
  attachment.data = base_table.cog_oid ;
