#!/usr/bin/env python
#-*- coding: utf-8 -*-

from collorg.controller.web import WebController


db_name = '__DB_NAME__' # REPLACE WITH YOUR DATABASE NAME

wc = WebController(db_name)
def application(environ, start_response):
    return wc.process(environ, start_response)
