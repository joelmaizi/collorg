(function($) {
    var version = '0.0.28';
    var resizeTimer;
    $.cog = {};
    $.cog.trace = false;
    $.cog.pages = '#cog_container>section.page';
    $.cog.active_page = $.cog.pages + '.active';
    $.cog.active_page_link = $.cog.active_page + '>header>div.title>a';
    $.cog.history = '#cog_menu>ul';
    $.cog.perm_link = '#cog_perm_link>ul'
    $.cog.history_items = $.cog.history + '>li';
    $.cog.site_map_link = '#cog_top_nav_map>a'

    var creole = new Parse.Simple.Creole({
        forIE: document.all,
        interwiki: {
            WikiCreole: 'http://www.wikicreole.org/wiki/',
            Wikipedia: 'http://en.wikipedia.org/wiki/'
        },
        linkFormat: ''
    });

    $.fn.page_title_link = function(page_ref){
        return $().get_page(page_ref).find('header>.title>a');
    }

    $.fn.add_site_map_to_history = function(){
      $.cog.trace && console.log('$.add_site_map_to_history()');
      site_map_link = $($.cog.site_map_link).clone();
      $($.cog.perm_link).append(site_map_link);
      $($.cog.perm_link + '>a').wrap('<li class="focus"></li>');
    }

    $.fn.append_to_history = function(page_ref){
      $.cog.trace && console.log('$.append_to_history('+page_ref+')');
      old_focus = $($.cog.history_items+'.focus');
      $($.cog.history_items).removeClass('focus');
      deja_vu_link = $($.cog.history_items+">a[page_ref='"+page_ref+"']")

      if(deja_vu_link.length){
          $(deja_vu_link).parent().addClass('focus');
          position = $(deja_vu_link).parent().position();
      } else
      {
          $(old_focus).nextAll().remove();
          link = $().page_title_link(page_ref).clone()
            .removeClass('force_reload').addClass('d_see_also');

          $($.cog.history).append(link);
          $($.cog.history + '>a').wrap('<li class="focus"></li>');
          position = $($.cog.history_items+">a[page_ref='"+page_ref+"']")
            .parent().position();
      }
      $('#cog_menu').scrollTop(position.y);
    }

    $.fn.change_focus_history = function(page_ref){
      $.cog.trace && console.log('$.change_focus_history('+page_ref+')');
      $($.cog.history_items).removeClass('focus');
      $($.cog.history_items + ">a[page_ref='"+page_ref+"']")
        .parent().addClass('focus');
    }

    $.fn.get_active_page_ref = function(){
        var page_ref = $($.cog.active_page+">header>.title>a").get_page_ref();
        $.cog.trace && console.log('$.get_active_page_ref()->'+page_ref);
        return page_ref;
    }

    $.fn.get_page_ref = function(){
      var page_ref = $(this).attr('page_ref');
      $.cog.trace && console.log('$.get_page_ref()->'+page_ref);
      return page_ref;
    }

    $.fn.get_page = function(page_ref){
      $.cog.trace && console.log('$.get_page('+page_ref+')->$(page)');
      return $('#' + page_ref);
    }

    $.fn.page_exists = function(page_ref){
      ret_val = $.fn.get_page(page_ref).length != 0
      $.cog.trace && console.log('$.page_exists('+page_ref+')->'+ret_val);
      return ret_val
    }

    $.fn.new_empty_page = function(page_ref){
      $.cog.trace && console.log('$.new_empty_page('+page_ref+')->$(page)');
      $('#cog_container')
        .append('<section id="' + page_ref + '" class="page"></section>');
      return $(this).get_page(page_ref);
    }

    $.fn.reload_page = function(page_ref){
        $.cog.trace && console.log('$.reload_page('+page_ref+')->$(page)');
        $('#cog_container')
          .append('<section id="' + page_ref + '" class="page"></section>');
        return $(this).get_page(page_ref);
    }

    $.fn.activate_page = function(page_ref){
      $.cog.trace && console.log('$.activate_page()->$(this)\n'+page_ref);
      $($.cog.pages).removeClass('active').addClass('hidden');
      $(this).removeClass('hidden').addClass('active');
      if(!_cog_connected()){
          $(this).addClass('anonymous');
      }else{
          if($(this).hasClass('anonymous')){
              $(this.removeClass('anonymous'));
              $($().page_title_link(page_ref)).click();
          }
      }
      $().append_to_history(page_ref);
      return $(this)
    }

    $.fn.cog_resize = function()
    {
      $.cog.trace && console.log('cog_resize()');
      return $(this).each(function(){
        $.cog.header_height_ = $('#header').outerHeight(true);
        $.cog.footer_height_ = $('#footer').outerHeight(true);
        $.cog.height_ = $(window).height() -
          ($.cog.header_height_ + $.cog.footer_height_);
        $().cog_pageHeight();
      });
    }

    $.fn.cog_pageHeight = function()
    {
      $.cog.trace && console.log('cog_pageHeight()');
        var height_ = $.cog.height_ - 4;
        $('#cog_main').css('height', height_ + 'px');
        return $($.cog.active_page).each(function(){
            var h_fixed_elts = 0;
            $(this).css('height', height_);
            $(this).children().not('article').each(function(){
                h_fixed_elts += parseInt($(this).outerHeight(true));
            });
            $(this).find('article').first().css(
                'height', height_ - (h_fixed_elts) - 10 + 'px');
            $('#cog_menu').css('height', (height_ / 2));
            $('#cog_cart').css('height', (height_ / 2));
            $('.trace_external').css({height:$(
                ".trace_external").parent().height()});
            $('section.page').children(".trace").css(
                {'height':$(this).parent().height()});
        });
    }

    $.fn.updateMathGlyphs = function()
    {
      $.cog.trace && console.log('updateMathGlyphs()');
        MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
    }

    function __check_cookies()
    {
      $.cog.trace && console.log('__check_cookies()');
        try{
            var $S = window.localStorage;
            $('.no_cookies').addClass('hidden');
            $('.cookies').removeClass('cookies');
        }catch(err){
            console.log('need cookies');
        }
        return $S;
    }

    function trim()
    {
      $.cog.trace && console.log('trim()');
        return String(this).replace(/^\s*|\s*$/g, '');
    }

    function exists(elt)
    {
        ret_val = elt && elt.length > 0;
      $.cog.trace && console.log('exists()->'+ret_val);
        return ret_val;
    }

    function random_id()
    {
      $.cog.trace && console.log('random_id()');
        var rand1 = Math.ceil(Math.random() * 1000000);
        var rand2 = Math.ceil(Math.random() * 1000000);
        var rand_id = 'id_' + rand1 + '_' + rand2;
        return rand_id;
    }

    function _close(elt)
    {
      $.cog.trace && console.log('_close()');
        $($(elt).attr('target')).html("");
    }

    function _switch_loaded_link(elt)
    {
      $.cog.trace && console.log('_switch_loaded_link()');
        var rand_id = random_id()
        var refresh_link = $(elt).clone();
        $(refresh_link)
            .addClass('button small left')
            .attr('target', '#' + rand_id)
            .html('<img class="icon" alt="reload" title="reload" ' +
                  'src="/collorg/images/refresh.svg" />').after('&nbsp;');
        $(elt).after(
            '<div id="' + rand_id + '" class="page">' + '</div>');
        $(elt).before($(refresh_link));
        var a_label = $(elt).html();
        $(elt).replaceWith(
            '<span class="link toggle_delt">' + a_label + '</span> '
        );
        return $('#' + rand_id)
    }

    function _get_target(elt)
    {
      $.cog.trace && console.log('_get_target()');
        /* returns the target referenced by the attr 'target'
         *
         * this_, after_, previous_, parent_
         * an explicit id: #something
         */
        var special_targets = [
            '_page_article_', '_page_', '_reset_', 'this_',
            '_after_', 'previous_', 'next_', 'parent_'];
        var target = $(elt).attr('target') || undefined;
        if(target && $.inArray(target, special_targets) == -1 &&
           target[0] != '#'){
            target = '#' + target;
            if($(target).length == 0){
                console.log("Wrong target " + target);
            }
        }
        switch(target) {
          case 'this_':
              return $(elt);
          case '_page_article_':
              return '#' + $($.cog.pages)
                .not('.hidden').find('article').first().attr('id');
          case '_reset_':
              $('#cog_container').empty();
              return undefined;
          case '_page_':
            return '#' + $($.cog.pages)
                .not('.hidden').attr('id');
          case '_after_':
              // need a better random ID.
              return _switch_loaded_link($(elt));
          case 'previous_':
              return $(elt).prev().show();
          case 'next_':
              return $(elt).next().show();
          case 'parent_':
              return $(elt).parent();
          default:
              return $(target).show();
        }
    }

    $.fn.map_ = function(posted_data)
    {
      $.cog.trace && console.log('map_()');
        /* récup. les informations dans <div id="#cog_data">...</div>.
           (string representation of a json object).
        */
        return this.each(function() {
            var json_data = $.parseJSON(posted_data);
            try{
                var cog_data = $.parseJSON($("#cog_data").html());
            } catch(err) {
                console.log(err);
            }
            if($(this).attr('target')) {
                json_data.cog_target = $(this).attr('target');
            }
            $.data(document.body, 'cog_data', json_data);
            $.each(cog_data, function(key, val){
                json_data[key] = val;
            });
            var cog_environment = $("#cog_environment").html()
            if(cog_environment != '') {
                json_data.cog_environment = cog_environment;
            }
        });
    }

    $.fn.post_modal = function(key)
    {
        return this.each(function(){
            var cont = $(this).html();
            var dmodal = key == '#cog_dialog_modal';
            $(this).dialog({
                modal: dmodal, hide: 'slide',
                title: $(cont).attr('title'),
                width: $(cont).attr('width'),
            });
        });
    }

    function __preview_wiki(elt)
    {
        var target = $($(elt).attr('target'));
        if(elt.hasClass('ewiki')){
            elt.addClass('hidden').prev().removeClass('hidden');
            target.removeClass('hidden').parent().next().addClass('hidden');
        } else {
            elt.addClass('hidden').next().removeClass('hidden');
            target.addClass('hidden').next();
            node = target.parent().next();
            $(node).html('').removeClass('hidden');
            creole.parse(node['0'], target.val());
            $(node).updateMathGlyphs();
        }
    }

    $.fn.wrapPage = function()
    {
      $.cog.trace && console.log('wrapPage()');
      page = "<section>" + $(this).html() + "</section>";
      $("#cog_container").html(page)
      page = $("#cog_container>section")
      page_ref = $(page).find("header>div.title>a").attr('page_ref')
      if(!page_ref){
          page_ref = 'page_ref_' + random_id();
      }
      $.cog.trace && console.log('wrapPage: page_ref: ' + page_ref);
      $(page).addClass('page active').removeClass('hidden')
        .attr('id', page_ref);
      return page_ref;
    }

    $.fn.post_result = function(data)
    {
      $.cog.trace && console.log('post_result()');
        return this.each(function() {
            var elt_target = _get_target(this);
            var dialog = false;
            $.each(data, function(key, val) {
                if(key.substr(0,11) == "#cog_dialog") {
                    dialog = true;
                }
                $('.cog_new_post_link').addClass('hidden');
                if(key[0]!=='#') {
                    key="#"+$(elt_target).attr("id");
                }
                if(key == '#cog_reload') {
                    $('body').data('cog_reload', '#cog_container');
                }
                if($(key).hasClass('reload')) {
                    $(key).replaceWith(val);
                } else {
                    if($(key).length != 0) {
                        if(key == '#cog_container') {
                            var page_ref = data['#page_ref'];
                            if(page_ref) {
                                if($().page_exists(page_ref)) {
                                  //reload
                                  $('#' + page_ref).html(val)
                                    .activate_page(page_ref);
                                } else {
                                  $().new_empty_page(page_ref)
                                    .html(val).activate_page(page_ref);
                                }
                            }
                        } else {
                            $(key).html(val);
                        }
                    }
                }
                if(key.substr(0,11) == "#cog_dialog"
                   && trim($(key).html()) != '') {
                    dialog = true;
                    $(key).post_modal(key);
                }
            });
            if(!dialog) {
                try{$("#cog_dialog_modal").dialog('close');}catch(err){}
            }
        });
    }

    $.fn.attach_ = function(data_form_id)
    {
      $.cog.trace && console.log('attach_()');
        var url = undefined;
        return this.each(function() {
            $.blockUI({ message:'',overlayCSS: { opacity: 0.1} });
            var form = $('#' + data_form_id);
            var uploader = $(form).attr('action');
            var target = $(form).find('input[name="cog_target"]').val();
            var formData = new FormData($(form)[0]);
            var data_ = $(':file');
            $.ajax({
                url: uploader,
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data)
                {
                    $('#callback_' + target).click();
                }
            });
        });
    }

    $.fn._cog_reloadAnonymousPage = function()
    {
      $.cog.trace && console.log('_cog_reloadAnonymousPage()');
      if(_cog_connected()) {
        $($.cog.active_page+'.anonymous')
          .removeClass('anonymous')
          .find('header>.title>a').first().click();
      } else {
        $($.cog.pages).addClass('anonymous');
      }
      return this;
    }

    $.fn.post_ = function()
    {
      $.cog.trace && console.log('post_()');
      page_ref = $(this).get_page_ref();
      active_page_ref = $().get_active_page_ref();
      if (page_ref &&
          page_ref != active_page_ref &&
          !$(this).hasClass('force_reload') &&
          $().page_exists(page_ref))
      {
          $().get_page(page_ref).activate_page(page_ref);
          return false;
      }
      connected = _cog_connected();
      return this.each(function(){
        force_reload = $(this).hasClass('force_reload');
        var elt = this;
        var link = this;
        $('body').css('cursor', 'wait');
        $.blockUI({ message:'',overlayCSS: { opacity: 0.1} });
        var posted_data = $(this).attr('data-cog') || '{}';
        $(this).map_(posted_data);
        $.cog.data_href = $(this).attr('data-href');
        $.cog.href = $(this).attr('href');
        $.ajax({
            type:'POST',
            url:$.cog.data_href || $.cog.href,
            data:$.data(document.body, 'cog_data'),
            success:function(data){
                if(data['#_error_']) {
                  alert(data['#_error_']);
                }
                try{
                    $(elt).post_result(data)
                    $(elt).parents('.page').find('a')
                        .removeClass('clicked').end();
                    $(elt).addClass('clicked');
                    $(elt).liveCalendar();
                    svgFallback();
                }catch(err){
                    console.log(err);
                }
            },
            complete:function(){
                $().cog_pageHeight();
              $($.cog.active_page+">article").scrollTop();
              if($($.cog.history_items).length == 0){
                $().add_site_map_to_history();
                page_ref = $().get_active_page_ref();
                $().get_page(page_ref).activate_page(page_ref);
              }
              if(!connected && _cog_connected()){
                  //reload the active page
                  $($.cog.active_page_link).click();
              }
                $(".moment_llll").each(function(){
                    $(this).text(
                        moment($(this).text()).format('llll')
                    );
                    $(this).removeClass('moment_llll');
                });
                $(".moment_ll").each(function(){
                    $(this).text(
                        moment($(this).text()).format('ll')
                    );
                    $(this).removeClass('moment_ll');
                });
                $($.cog.pages+'>header>div.title>a')
                  .addClass('force_reload');
                $('a:not([target])').attr('target', 'blank_');
                if($('body').data('cog_reload') != ''){
                    $('.reload').each(function(){
                        $(this).trigger('click');
                    });
                    $('body').data('cog_reload', '');
                }
                $('body').data('cog_reload', '');
                $('.datepicker').datepicker({
                    dateFormat: "yy-mm-dd",
                    appendText: "(yyyy-mm-dd)",
                    changeMonth: true,
                    changeYear: true,
                    showWeek: true
                });

                $('.timepicker').datetimepicker({
                    dateFormat: "yy-mm-dd",
                    changeMonth: true,
                    changeYear: true,
                    showWeek: true,
                    stepMinute: 15,
                    timeFormat: 'HH:mm:ss'
                });

                initDragEvents();
                $(".tabs").tabs({
                    load: function(event, ui){
                      ui.panel.initDragEvents();
                    }
                });
                $('select[trigger]').on('change', function(){
                    $(this).find('option:selected[triggered]')
                        .each(function(){
                            $($(this).attr('triggered')).trigger('click');
                    });
                    $(this).prop('selectedIndex', 0);
                });
                $('.trace, .trace_external').resizable({
                    containment: "parent",
                    grid: [ 100, 100 ]
                });
                try{
                    $($.cog.active_page).updateMathGlyphs();
                }catch(err){
                    console.log(err);
                }
            },
            dataType:'json',
        });
        $('body').css('cursor', 'auto');
      });
    }

    $.fn.serializeObject = function()
    {
      $.cog.trace && console.log('serializeObject()');
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined)
            {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            }
            else
            {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    $.fn.form_to_json = function()
    {
        var a_form = {};
        function isArray()
        {
            if (typeof arguments[0] == 'object') {
                var criterion = arguments[0]
                    .constructor.toString().match(/array/i);
                return (criterion != null);
            }
            return false;
        }
        $(this).find(':input').each(function(){
            var elt = this
            // .not(':checkbox') doesn't work
            if($(elt).attr('type') != 'checkbox' &&
               $(elt).attr('type') != 'radio'){
                var key = $(this).attr('name');
                var val = $(this).val();
                if(key != undefined){
                    a_form[key] = val;
                }
            }
        }).end().find(':checkbox:checked').each(function(){
            var key = $(this).attr( "name" );
            var val = $(this).val();
            if ( ! isArray( a_form[key] ) ) {
                a_form[key] = new Array;
            }
            a_form[key].push(val);
        });
        return $.toJSON(a_form);
    };

    $.fn.liveCalendar = function()
    {
        return this.each(function(){
            $(".calendar").each(function(){
                if($(this).html() != '')
                {
                    return;
                }
                var cog_oid = $(this).first().attr('id');
                $(this).fullCalendar({
                    events: '?cog_method=w3get_events&amp;' +
                        'cog_oid_=' + cog_oid + '&amp' +
                        'cog_ajax=true&cog_raw=true',
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay'
                    },
                    eventClick: function(calEvent,jsEvent,view){
                        var url_event = '?cog_oid_=' + calEvent.cog_oid +
                            '&amp;cog_method=w3display&amp;cog_ajax=true' +
                            '&amp;cog_target=event';
                        var result = $.getJSON(url_event, function(data) {
                            $("#cog_dialog_modal").html(data.event.content)
                                .post_modal();
                        });
                    },
                    defaultView: 'agendaWeek',
                    minTime: 7,
                    maxTime: 21,
                    firstDay: 1,
                });
            });
        });
    };

    $.fn.getOidFromUrl = function()
    {
      $.cog.trace && console.log('getOidFromUrl()');
        return $.url($(this).attr('data-href')).param('cog_oid_');
    }

    function __follow(item)
    {
      $.cog.trace && console.log('__follow()');
        var here = false;
        $("#cog_cart").find("li.background").remove();
        $("#cog_cart").find('li>a').each(function(){
            if($(this).attr('href') == $(item).attr('href')){
                here = true;
            }
        });
        if(here) return;

        var followed = $(item)
            .clone()
            .draggable({appendTo: 'body',helper:'clone', iFrameFix: true})
            .addClass('action d_see_also')
            .html(item.html());

        var del = $('<a></a>')
            .addClass('cart-remove-action')
            .attr('onclick','$(this).parent().remove();');

        followed.attr('target', '#cog_container');

        $('<li style="position:relative"></li>')
            .append(del).append(followed).appendTo("#cog_cart>ul");
    }

    /**
     * Initialize all the dom components
     * an droppable events.
     */
    function __init()
    {
      $.cog.trace && console.log('__init()');
      $(document).keydown(cog_keyHandler);
        $('.no_javascript').addClass('hidden');
        $('.javascript').removeClass('javascript');
        page_ref = $("#cog_container").wrapPage();
        $().add_site_map_to_history();
        $().get_page(page_ref).activate_page(page_ref);
        $(window).cog_resize();
        if(window.location.search)
        {
            $('#cog_home_link').hide();
        }
        __check_cookies();
        svgFallback();
        initDragEvents();

        $(".tabs").tabs({
          load: function(event, ui){
            ui.panel.initDragEvents();
          }
        });

        $( "#cog_cart ul" ).droppable({
            activeClass: "ui-state-default",
            hoverClass: "ui-state-hover",
            tolerance: "pointer",
            accept: ".action",
            drop: function( event, ui )
            {
                __follow(ui.draggable);
            },
        });
    }

    function initDragEvents()
    {
      $.cog.trace && console.log('initDragEvents()');
        $('.action').draggable({
          appendTo: 'body',
            distance:15,
            helper:"clone",
            zIndex:30
        });
        $('.wiki > textarea').droppable({
               activeClass: "ui-state-default",
               hoverClass: "ui-state-hover",
               tolerance: "touch",
               accept: ".d_see_also",
               drop: function(event, ui)
               {
                   __addRefToWiki(event,ui.draggable);
               }
        });
    }

    $.fn.initDragEvents = initDragEvents;

    function _cog_connected()
    {
        var session = $.cookie('cog_session');
        if(session === undefined || session == null){
            return false;
        }
        return true;
    }

    /**
     * will check if the user have a session cookie
     * and in this case, will try to log-in the
     * user.
     */
    function _try_to_log_user()
    {
      $.cog.trace && console.log('_try_to_log_user()');
        if($.cookie)
        {
            var session = $.cookie('cog_session');
            if(session === undefined || session == null)
                return false;
            else {
                var link = $( '<a class="action hidden" ' +
                              'href="?cog_method=w3session_login' +
                              '&cog_fqtn_=collorg.actor.user"></a>' );
                link.post_(true);
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    function __addRefToWiki(event, item)
    {
        try{
            item = $(item.get(0));
            var area = $(event.target).get(0);

            var text = '<<cog_href>>'
                + $(item).getOidFromUrl() + '|' +
                item.text() + '<</cog_href>>';


            var scrollPos = area.scrollTop;
            var strPos = 0;
            var br = ((area.selectionStart||area.selectionStart=='0')?"ff": (
                document.selection ? "ie" : false ) );
            if (br == "ie") {
                area.focus();
                var range = document.selection.createRange();alert(area);
                range.moveStart ('character', -(area.value.length));
                strPos = range.text.length;
            } else if (br == "ff")
                strPos = area.selectionStart;
            var front = (area.value).substring(0, strPos);
            var back = (area.value).substring(strPos, area.value.length);
            area.value=front+text+back;
            strPos = strPos + text.length;
            if (br == "ie") {
                area.focus();
                var range = document.selection.createRange();
                range.moveStart ('character', -(area.value.length));
                range.moveStart ('character', strPos);
                range.moveEnd ('character', 0);
                range.select();
            } else if (br == "ff") {
                area.selectionStart = strPos;
                area.selectionEnd = strPos;
                area.focus();
            }
            area.scrollTop = scrollPos;

        } catch(err) {
            console.log(err);
        }
        return false;
    }

    /**
     * Manages the display of svg items and it's fallbacks if
     * the current browser do not support them.
     */
    function svgFallback()
    {
        /**
         * @return True if the current browser supports svg
         */
        // We add a [no-svg] class to the document
        // and we replace all svg items in <img> tags
        // with a png.
        if(navigator.userAgent.search("MSIE") == -1){
            return;
        }
        document.documentElement.className += ' no-svg';
        var imgs = document.getElementsByTagName('img');
        var dotSVG = /.*\.svg$/;
        for (var i = 0; i != imgs.length; ++i) {
            if (imgs[i].src.match(dotSVG)) {
                imgs[i].src = imgs[i].src.slice(0, -3) + 'png';
            }
        }
    }

    cog_keyHandler = function(evt){
        var code = evt.keyCode || evt.which;
        $.cog.trace && console.log('cog_keyHandler('+code+')');
        var post_item;
        if($(evt.target).is(':input')){
            var target = $(evt.target);
            return;
        }
        var link = $($.cog.history_items+'.focus');
        if(evt.which == $.ui.keyCode.LEFT){
            prev = $(link).prev();
            if(prev.length == 0){
                prev = $($.cog.history_items).last();
            }
            $(prev).find('a').click();
            evt.preventDefault();
        } else if(evt.which == $.ui.keyCode.RIGHT){
            next = $(link).next();
            if(next.length == 0){
                next = $($.cog.history_items).first();
            }
            $(next).find('a').click();
            evt.preventDefault();
        }
    }

    /**
     * Main function
     */
    $.fn.collorg = function()
    {
      $.cog.trace && console.log('collorg()');
        __init();
        $('body').data('cog_reload', '');
        $.data(document.body, 'cog_data_form', {ajax:true});

        $('.toggle').click(function(){
            var to_toggle = $(this).attr('to_toggle');
            var elt = $($(this).attr('to_toggle')) || $(this);
            elt.toggle('slow', function(){
                $(this).cog_resize();
            });
        });

        $(document).on('click',function(evt){
            var target = $(evt.target);
            if(target.hasClass('ewiki') || target.hasClass('vwiki')){
                __preview_wiki(target);
                return false;
            }
            if(target.hasClass('action') || target.parent().is('a.action')) {
                if(!target.hasClass('action')) {
                    target = target.parent();
                }
                evt.preventDefault();
                var elt = target;

                if(target.attr('trigger')) {
                    elt = $(target.attr('trigger')).trigger('click');
                    return false;
                }

                if($(elt).attr('data-form-id'))        {
                    var data_form_id = $(elt).attr('data-form-id');
                    var form = $('#'+data_form_id);
                    $(elt).attr('data-cog', $(form).form_to_json());
                    $(elt).post_();
                    return false;
                } else {
                    if(exists($(elt).attr('target'))
                       && $(elt).attr('target') != 'blank_'
                       && $(elt).is('a')) {
                        if($(elt).parent().hasClass("post_item")){
                            var post_item = $(elt).parent();
                            post_item.siblings('.clicked').removeClass('clicked');
                            post_item.addClass('clicked');
                            evt.stopImmediatePropagation();
                        }
                        if($(elt).hasClass('close')) {
                            _close($(elt));
                            evt.preventDefault();
                        } else {
                            if($(elt).parents('.post_item').length > 0) {
                                var post_item = $(elt).parents(
                                    '.post_item:first');
                                post_item.siblings('.clicked').removeClass(
                                    'clicked');
                                post_item.addClass('clicked');
                            }
                            $(elt).post_();
                            return false;
                        }
                        evt.preventDefault();
                    } else if($(elt).is('a')) {
                        // no ajax if target does not exist or if it is 'blank_'
                        $(elt).attr('target', 'blank_');
                    }
                }
            } else if(target.hasClass('attachment')) {
                var data_form_id = target.attr('data-form-id');
                target.attach_(data_form_id);
                return false;
            } else if(target.hasClass('post_item')) {
                target.siblings('.clicked').removeClass('clicked');
                target.addClass('clicked');
                if (target.is('a')) return; //stop bubbling
                evt.preventDefault();
                target.siblings('.focus').removeClass('focus');
                the_target = target.addClass('focus').find('a:first').trigger('click');
                return false;
            } else if(target.parents('.post_item').length > 0) {
                var post_item = target.parents('.post_item:first');
                post_item.siblings('.clicked').removeClass('clicked');
                post_item.addClass('clicked');
                if (target.is('a')) return; //stop bubbling
                evt.preventDefault();
                post_item.siblings('.focus').removeClass('focus');
                post_item.addClass('focus').find('a:first').trigger('click');
                return false;
            }
        });

        $(document).ajaxStop(ajax_stop);
        $(window).resize(function(){
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout($(window).cog_resize, 250);
        });
        _try_to_log_user();
    };
})(jQuery)
