function insertAtCaret(area, text) {
    var scrollPos = area.scrollTop;
    var strPos = 0;
    var br = ((area.selectionStart || area.selectionStart == '0') ? "ff" : (
	document.selection ? "ie" : false ) );
    if (br == "ie") {
        area.focus();
        var range = document.selection.createRange();alert(area);
        range.moveStart ('character', -(area.value.length));
        strPos = range.text.length;
    } else if (br == "ff")
        strPos = area.selectionStart;
    var front = (area.value).substring(0, strPos);
    var back = (area.value).substring(strPos, area.value.length);
    area.value=front+text+back;
    strPos = strPos + text.length;
    if (br == "ie") {
        area.focus();
        var range = document.selection.createRange();
        range.moveStart ('character', -(area.value.length));
        range.moveStart ('character', strPos);
        range.moveEnd ('character', 0);
        range.select();
    } else if (br == "ff") {
        area.selectionStart = strPos;
        area.selectionEnd = strPos;
        area.focus();
    }
    area.scrollTop = scrollPos;
}

$(document).ready(function() {
    var options = {
        accept: "span.placeholder",
        drop: function(ev, ui) {
            insertAtCaret($("textarea#main_text").get(0), ui.draggable.eq(0).text());
        }
    };

    $("span.placeholder").draggable({
        helper:'clone',
        start: function(event, ui) {
            var txta = $("textarea#main_text");
            $("div#pseudodroppable").css({
                position:"absolute",
                top:txta.position().top,
                left:txta.position().left,
                width:txta.width(),
                height:txta.height()
            }).droppable(options).show();
        },
        stop: function(event, ui) {
            $("div#pseudodroppable").droppable('destroy').hide();
        }
    });
});
