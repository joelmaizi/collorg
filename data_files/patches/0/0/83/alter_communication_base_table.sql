alter table "collorg.communication".base_table add column title string ;
alter table "collorg.communication".base_table add column text wiki ;
alter table "collorg.communication".base_table add column introductory_paragraph string ;
alter table "collorg.communication".base_table add column author c_oid ;
alter table "collorg.communication".base_table add column visibility string ;
