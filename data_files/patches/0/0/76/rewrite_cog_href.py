#!/usr/bin/env python
#-*- coding: utf-8 -*-

import re
from collorg.controller.controller import Controller

ctrl = Controller()
db = ctrl.db
relation = db.relation

posts = relation('collorg.communication.blog.post')
posts.text_.value = '%<<cog_href>>%', 'like'
for post in posts:
    text = post.text_.value
    res = []
    for elt in text.split('<<cog_href>>'):
        if not '<</cog_href>>' in elt:
	    res.append(elt)
	else:
	    href, suite = elt.split('<</cog_href>>')
            href, data_href, page_ref, target, label = href.split('|')
            cog_oid = page_ref.replace('w3display-', '')
	    res.append('<<cog_href>>{}|{}<</cog_href>>'.format(cog_oid, label))
            res.append(suite)
    npost = post()
    npost.text_.value = ''.join(res)
    post.update(npost, update_modif_date=False)
