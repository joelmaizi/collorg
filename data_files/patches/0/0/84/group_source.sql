CREATE TABLE "collorg.group"."group_source" (
   cog_oid C_OID UNIQUE NOT NULL,
   FOREIGN KEY(cog_oid) REFERENCES "collorg.core".oid_table(cog_oid)
      INITIALLY DEFERRED,
   cog_fqtn C_FQTN
      DEFAULT 'collorg.group.group_source'
      CHECK( cog_fqtn = 'collorg.group.group_source' ),
   name STRING,
   contact c_oid references "collorg.actor"."user"(cog_oid),
   PRIMARY KEY(name)
) INHERITS("collorg.communication".base_table) ;
