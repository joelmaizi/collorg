alter table "collorg.core".data_type add column tablekind char default 'r'
  check(tablekind = 'r' or tablekind = 'v');
