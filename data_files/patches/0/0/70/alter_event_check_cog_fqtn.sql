alter table "collorg.event".event drop constraint event_cog_fqtn_check ;
alter table "collorg.event".event add constraint "event_cog_fqtn_check" CHECK (cog_fqtn::text = 'collorg.event.event'::text) no inherit ;
