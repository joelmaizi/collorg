drop view "collorg.access.view".topic_access ;
drop table "collorg.access".a_topic_function ;
delete from "collorg.core".field where fqfn like 'collorg.access.a_topic_function%';
delete from "collorg.core".data_type where fqtn = 'collorg.access.a_topic_function';
delete from "collorg.core".field where fqfn like 'collorg.access.view.topic_access%';
delete from "collorg.core".data_type where fqtn like 'collorg.access.view.topic_access%';
drop table "collorg.access".indirect ;
delete from "collorg.core".field where fqfn like 'collorg.access.indirect%';
delete from "collorg.core".data_type where fqtn like 'collorg.access.indirect%';
