create table "collorg.application".action_duration (
    "begin" timestamp(0),
    "end" timestamp(0),
    action c_oid,
    foreign key (action) references "collorg.application".action(cog_oid),
    primary key("begin", "end", action)
);
