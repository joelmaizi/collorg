alter table "collorg.communication".a_tag_post no inherit "collorg.core".base_table;
alter table "collorg.communication".a_tag_post drop column cog_oid ;
alter table "collorg.communication".a_tag_post drop column cog_fqtn ;
alter table "collorg.communication".a_tag_post drop column cog_signature ;
alter table "collorg.communication".a_tag_post drop column cog_test ;
alter table "collorg.communication".a_tag_post drop column cog_creat_date ;
alter table "collorg.communication".a_tag_post drop column cog_modif_date ;
alter table "collorg.communication".a_tag_post drop column cog_environment ;
alter table "collorg.communication".a_tag_post drop column cog_state ;
