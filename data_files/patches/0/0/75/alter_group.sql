-- communication.blog.post
alter table "collorg.group"."group" add column   title STRING not null default '';
alter table "collorg.group"."group" add column   introductory_paragraph STRING default '';
alter table "collorg.group"."group" add column   text WIKI NOT NULL default '';
alter table "collorg.group"."group" add column   author C_OID ;
alter table "collorg.group"."group" add    constraint "author" FOREIGN KEY (author) REFERENCES "collorg.actor"."user"(cog_oid);
alter table "collorg.group"."group" add column   public BOOLEAN DEFAULT 'f';
alter table "collorg.group"."group" add column   comment BOOLEAN DEFAULT 'f';
alter table "collorg.group"."group" add column   expiry_date timestamp(0);
alter table "collorg.group"."group" add column   important boolean default 'f';
alter table "collorg.group"."group" add column   broadcast boolean default 'f';
alter table "collorg.group"."group" add column   visibility STRING NOT NULL CHECK (visibility IN ('private', 'protected', 'public')) default 'protected';
-- event.event
--alter table "collorg.group"."group" add column  begin_date TIMESTAMP(0) NOT NULL default now()::date;
--alter table "collorg.group"."group" add column   end_date TIMESTAMP(0);
--alter table "collorg.group"."group" add column   location C_OID;
--alter table "collorg.group"."group" add column   location_str string;
--alter table "collorg.group"."group" add constraint location_fkey FOREIGN KEY(location) REFERENCES "collorg.location".room(cog_oid);
--alter table "collorg.group"."group" add column   other_location WIKI;
--
alter table "collorg.group"."group" rename CONSTRAINT group_visibility_check to visibility_visibility_check;
alter table "collorg.group"."group" inherit "collorg.communication.blog".post;
