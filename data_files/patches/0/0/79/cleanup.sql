delete from "collorg.core".field where fqfn like 'collorg.actor.relation%';
delete from "collorg.core".data_type where fqtn like 'collorg.actor.relation%';
drop table "collorg.actor".relation;

delete from "collorg.application".a_action_task where action in (
select cog_oid from "collorg.application".action
where data_type = 'collorg.planning.calendar');
delete from "collorg.application".action where data_type = 'collorg.planning.calendar';

delete from "collorg.core".field where fqfn like 'collorg.planning.%';
delete from "collorg.core".data_type where fqtn like 'collorg.planning.%';
drop table "collorg.planning".calendar;
drop table "collorg.planning".task;
drop table "collorg.planning".oid_table;
drop schema "collorg.planning";


delete from "collorg.core".field where fqfn like 'collorg.i18n%';
delete from "collorg.core".data_type where fqtn like 'collorg.i18n.%';
drop table "collorg.i18n".translation;
drop table "collorg.i18n".language;
drop table "collorg.i18n".oid_table;
drop schema "collorg.i18n";

delete from "collorg.core".field where fqfn like 'collorg.web.wall%';
delete from "collorg.core".data_type where fqtn like 'collorg.web.wall%';
drop table "collorg.web".wall;

delete from "collorg.core".field where fqfn like 'collorg.web.a_rss_topic%';
delete from "collorg.core".data_type where fqtn like 'collorg.web.a_rss_topic%';
drop table "collorg.web".a_rss_topic;

delete from "collorg.application".a_action_task where action in (
select cog_oid from "collorg.application".action where data_type = 'collorg.web.rss');
delete from "collorg.application".action where data_type = 'collorg.web.rss';
delete from "collorg.core".field where fqfn like 'collorg.web.rss%';
delete from "collorg.core".data_type where fqtn like 'collorg.web.rss%';
drop table "collorg.web".rss;

delete from "collorg.core".field where fqfn like 'collorg.communication.poll%';
delete from "collorg.core".data_type where fqtn like 'collorg.communication.poll%';
drop table "collorg.communication".poll;

