alter table "collorg.communication".comment no inherit "collorg.core".base_table ;
alter table "collorg.communication".comment inherit "collorg.communication".base_table ;
alter table "collorg.communication".file no inherit "collorg.core".base_table ;
alter table "collorg.communication".file inherit "collorg.communication".base_table ;
alter table "collorg.communication.blog".post no inherit "collorg.core".base_table ;
alter table "collorg.communication.blog".post inherit "collorg.communication".base_table ;
alter table "collorg.communication.blog".a_post_data no inherit "collorg.core".base_table ;
alter table "collorg.communication.blog".a_post_data inherit "collorg.communication".base_table ;
