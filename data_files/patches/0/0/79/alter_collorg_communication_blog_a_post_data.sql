alter table "collorg.communication.blog".a_post_data drop constraint a_post_data_post_fkey ;
alter table "collorg.communication.blog".a_post_data add constraint a_post_data_post_fkey foreign key(post) references "collorg.communication".oid_table(cog_oid);
alter table "collorg.communication.blog".a_post_data drop constraint a_post_data_data_fkey ;
alter table "collorg.communication.blog".a_post_data add constraint a_post_data_data_fkey foreign key(data) references "collorg.communication".oid_table(cog_oid);
