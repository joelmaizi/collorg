CREATE TABLE "collorg.communication".base_table (
   cog_oid C_OID UNIQUE NOT NULL,
   FOREIGN KEY(cog_oid) REFERENCES "collorg.core".oid_table(cog_oid)
      INITIALLY DEFERRED,
   cog_fqtn C_FQTN
      DEFAULT 'collorg.communication.base_table'
      CHECK( cog_fqtn = 'collorg.communication.base_table' and
      	     cog_fqtn is null ) no inherit
)INHERITS("collorg.core".base_table);
