delete from "collorg.actor".a_function_inst_group ;
delete from "collorg.actor".inst_group ;
delete from "collorg.core".field where fqfn like 'collorg.actor.%inst_group.%';
delete from "collorg.core".data_type where fqtn like 'collorg.actor.%inst_group%';
drop table "collorg.actor".a_function_inst_group;
drop table "collorg.actor".inst_group;
