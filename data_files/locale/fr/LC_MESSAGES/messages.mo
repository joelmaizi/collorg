��    �      �    <      �  ,  �  2  �  �     L   �     >  "   G     j     {     �  G   �     �  �   �     �     �  
   �  	   �     �     �  
   �     �  
               
   %     0     A     T     ]     i  (   y  	   �     �     �  
   �     �     �     �  h     0   x  /   �  9   �  
             '     -     9  M   >     �     �  
   �     �  	   �     �     �     �     �          -     A     U     h     n     }     �     �     �  	   �     �     �     �  
   �  	   �  	   �  	                  -     ;     D     L     Y     a     x     �     �     �     �     �     �     �  	   �     �     �          
       K   '     s  	   �     �     �     �  	   �     �     �  
   �  
   �                !     7  #   ?     c     r     �     �     �     �     �     �  
   �     �     �               -     B     U     i     r     w     ~     �     �     �  	   �     �     �     �     �     �  $   �  
             %     ,     1     6  	   L     V     \  N   _     �  
   �  
   �  {   �  ?   Q   F   �      �   #   �   I   !     U!     j!  O   �!     �!     �!     �!     �!     �!      "     "  
   "     !"     ."     6"     ;"     S"  
   X"  +   c"     �"  
   �"     �"     �"     �"     �"     �"     �"     �"     �"     �"  	   �"     �"     #  	   #     ##     +#     3#     I#     R#     Y#     g#     w#  	   ~#     �#     �#  	   �#     �#     �#     �#  @  �#  \  %  Q  c&    �'  a   �(     ))  3   0)     d)     z)     �)  V   �)     �)  |   *     �*     �*     �*     �*     �*     �*     +     +     +     "+     *+     1+     7+     F+     U+     \+     o+  8   �+     �+     �+     �+  	   �+     �+     ,     %,  �   <,  9   �,  @   -  P   E-  
   �-     �-     �-     �-     �-  w   �-     C.     L.     S.     [.  	   c.  #   m.  "   �.     �.     �.     �.  (   /     ./     N/     m/     t/     �/     �/     �/     �/     �/     �/     �/     �/     0     "0     &0     90     F0  %   \0      �0     �0     �0     �0     �0     �0     �0     �0     1     %1     -1  
   @1     K1  
   \1     g1     s1     1     �1     �1     �1  O   �1  #   2     ?2     R2     l2     ~2     �2     �2     �2     �2     �2     �2     �2     3  	   ,3  A   63     x3  +   �3     �3  	   �3     �3     �3  $   �3  
   4  	   %4     /4     O4  (   b4  !   �4     �4  #   �4     �4     5     5  	   %5     /5     C5     [5     y5     �5     �5     �5     �5     �5     �5  &   �5     6      6  	   &6  
   06     ;6     A6  
   V6     a6     g6  �   t6     7     7     /7  �   ;7  U   �7  P   48     �8  *   �8  ?   �8     9  #   9  d   @9     �9     �9     �9     �9     �9     �9     :  	   :      :     1:     8:  "   A:     d:     g:  &   u:     �:     �:     �:     �:     �:     �:     �:      ;     ;     ;     ';     .;     7;     J;     Q;     i;     u;     };     �;  
   �;     �;     �;     �;     �;     �;     �;     <     <     <     ,<         �   �   ;   8   �           .          >   �   �   y   �   d   J   �                  �       /   w         �       n       �   ?   �   5   �   {      �      T      �   b      ^   �   P   �   �             �   �      3   �   �   (   �   �   O   _   S       �   �   G   i       m   �   R   F   o   �       "   K   �   �       g   1   9   �      �       }   �      �   @   l       �   *   f       �   �   x          �   �   �   $       !   �   ~           #   Z   c   �                   p       �   `       �       7       �           V   �      ,   �   4          +      \   &       �       0   �   a   Q       �   u             %   �   �       �   �       N      Y   C              �      6               )       ]               	           �   :   �   �   �   �   �   s       W   j   �   I   
         2      �   �       �   �       -         �   �   �   �   B   q   [               �       =           A   �   |   �   '       �       h   M   �   �   �   �   v   t   e      �   �       �   H   <       �       U           X   r   D       �   k               z   L      �   E            
        You have created a new account on {}.
        To validate this account please login with the following information:
        - url: {}
        - identifier: {}
        - password: {}

        To change your password, click on "My desk" and then on the tab
        "Account settings".
         
        {} has created a new account for you on {}.
        To validate this account please login with the following information:
        - url: {}
        - identifier: {}
        - password: {}

        To change your password, click on "My desk" and then on the tab
        "Account settings".
         
    * To create a new document, click on the icon "Pencil+" (found above).
    * To attach an existing document, drag&drop its link in the zone (left handside). {{/collorg/images/cart.png}}
    Then pick it up again and drop it here.
     
    You can create or attach a directory in this document if you wish.
     Accesses Accessible by connected users only Account settings Add a group Add a person Add a reference by dragging a link from the post-it on the bottom-left. Add new Add posts on your desk by bookmarking them using the
        <img class="medicon" src="/collorg/images/bookmark.svg"> icon on the
        post and follow the comments under the "Comments" tab. All comments All documents All events All files Allow comments Attach a file Attachment Author Begin date Blog edition Bookmark Bookmarked Bookmarked posts Bookmarked removed Calendar Cancel edit Change password Check the bookmarked post on the desk of Clipboard Closed since Comments Connection Current password Documents associated to {} Documents attached to Drag & drop the link from the cart to 
the page/textarea to add a reference.
(write access is mandatory) Drag & drop your links here for future reference Drop a "post" here to publish it in this folder Drop a link from the cart to add a reference in the text. Empty list End date Event Expiry date File File attachement and post references can only be made once the post is saved. Files Filter First name Folder Follow-up For edition, please contact For the edition, please contact Futur events Grant access to a group Grant access to a user Grant attach access Grant manage access Grant write access Group Group activity Group comments Group documents Group files Groups Home page Icon Introductory paragraph Last comments Last month Last name Last week Last year Latest comments List my comments List my posts Location Manager Mark as read Members Members inherited from Members list Members of this post Members only Message More information My calendar My comments My desk My groups My posts Navigation history New New comment New comment form New comments/follow-ups on my posts/comments or the posts I have bookmarked New comments/follow-ups on {} New event New files on {} New follow-up New password New topic No subtree down here Open Open next: Open until Other groups in Other recipients Password Modification Persons Please avoid accents in files names Please connect Please connect to access this Post Posts Posts associated to {} Public access Referenced by Register Restricted Retype new password Revoke access Revoke attach access Revoke group access Revoke manage access Revoke user access Revoke write access Rss feed Save Search See also Select a group Select a recipient Select a user Send mail Share Sign in Sign up Sign up form Site map Sorry. You don't have access to this Strength:  Subject Submit Tags Text This folder is empty. This post Title To To remove a post from your desk, click on it, then click on the bookmark icon. Toggle selection Unregister Visibility Warning!

An error has occured. If you are trying to save a document,
please check that all mandatory fields are specified. Warning! Attachment is only working with the following browsers Warning! This post is private. Access will only be granted to members. Wrong password You don't have any personnal group. You will receive an email with the iCalendar file of this event attached. Your groups or units Your password has been changed Your password is in a LDAP directory. It cannot be changed by this application. attach access better by by tag by user check comma separated connection create group created edit emails, comma separated from group name group name or name of a member of the group in is missing last modified login logout manage the group members medium new password preview private protected protected document public read only refresh release remove from this list required search search result select an entry strong strongest to user last name very weak weak write access your home page Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-03-02 18:06+0100
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
        Vous avez demandé la création d'un compte sur {}.
        Pour le valider veuillez vous connecter en utilisant les informations suivantes :
        - url : {}
        - identifiant : {}
        - mot de passe : {}

        Pour changer de mot de passe, cliquez sur "Mon bureau", puis sur
        l'onglet "Paramètres du compte".         
        {} a créé votre nouveau compte sur {}.
        Pour le valider veuillez vous connecter en utilisant les informations suivantes :
        - url : {}
        - identifiant : {}
        - mot de passe : {}

        Pour changer de mot de passe, cliquez sur "Mon bureau", puis sur
        l'onglet "Paramètres du compte".         
    * Pour créer un nouveau document, cliquez sur l'icône "Crayon+" (ci-dessus).
    * Pour attacher un document existant, glissez/déposez son lien dans la zone ci-contre. {{/collorg/images/cart.png}}
    Puis reprenez le à partir de cette zone et déposez le ici.     
    Vous pouvez créer ou attacher un répertoire dans ce document
    si vous le souhaitez
     Accès Visible uniquement pour des utilisateurs connectés Paramètres du compte Ajouter un groupe Ajouter une personne Ajouter une référence ici en déposant un lien à partir du post-it en bas à gauche Ajouter un nouveau Suivez l'activité d'un billet en cliquant sur son icône de suivi <img class="medicon" src="/collorg/images/bookmark.svg">. Tous les commentaires Tous les documents Tous les événements Tous les fichiers Autoriser les commentaires Attacher un fichier Attachement Auteur Début Éditer Suivre Suivi Billets suivis Signet enlevé Agenda Annuler l'édition Modifier le mot de passe Retrouvez les posts que vous suivez via "Mon bureau" sur Presse papier Fermé depuis le Commentaires Connexion Mot de passe actuel Documents associés à {} Documents attachés à Glissez / déposez le lien du panier vers la 
page/zone de saisie pour ajouter une référence.
(le droit d'écriture sur la page est requis) Glissez / déposez vos liens ici pour un usage ultérieur Déposer le lien d'un "post" ici pour le publier dans ce dossier Déposez un lien à partir du caddie pour ajouter une référence dans le texte. Liste vide Fin Événement Date d'expiration Fichier L'attachement de fichiers et l'association d'autres posts seront possibles une fois que vous aurez sauvegardé ce post. Fichiers Filtre Prénom Dossier Répondre Pour l'édition, veuillez contacter Pour l'édition veuillez contacter Événements à venir Donner accès à un groupe Donner l'accès à une personne Attribuer le droit d'attacher un fichier Attribuer l'accès gestionnaire Attribuer le droit d'écriture Groupe Activité du groupe Commentaires du groupe Documents du groupe Fichiers du groupe Groupes Page personnelle Icône Paragraphe introductif Derniers commentaires Depuis un mois Nom Depuis une semaine Depuis un an Derniers commentaires Afficher la liste de mes commentaires Afficher la liste de mes billets Lieu Gestionnaire Marquer comme lu Membres Membres hérités de Liste des membres Membres du message Accès restreint aux membres Message Plus d'information Mon agenda Mes commentaires Mon bureau Mes groupes Mes billets Historique de navigation Nouveau Nouveau commentaire Nouveau commenatire de Nouveaux commentaires faits sur mes billets/commentaires ou les billets suivis. Nouveaux commentaires faits dans {} Nouvel événement Nouveaux fichiers dans {} Nouvelle réponse Nouveau mot de passe Nouveau dossier Pas de sous-arbre ici Ouvert Prochaine ouverture : Ouvert jusqu'au Autres groupes dans Autres destinataires Modification du mot de passe Personnes Merci d'éviter l'utilisation d'accents dans les noms de fichiers Veuillez vous connecter Veuillez vous connecter pour accéder à ce Billet Documents Documents associés à {} Accès public Documents référençant ce document S'inscrire Restreint Retapez le nouveau mot de passe Supprimer l'accès Supprimer le droit d'attacher un fichier Supprimer l'accès pour le groupe Supprimer l'accès gestionnaire Supprimer l'accès pour la personne Supprimer le droit d'écriture Flux RSS Enregistrer Recherche Documents associés Sélectionnez un groupe Sélectionner un destinataire Choisir une personne Envoyer le message Partager Connectez-vous Inscription Formulaire d'inscription Plan du site Désolé. Vous n'avez pas accès à ce Robustesse :  Sujet Soumettre Mots clefs Texte Ce dossier est vide. Ce message Titre Destinataire Pour enlever un billet de la liste ci-dessous, selectionnez-le puis  cliquez sur l'icône <img class="medicon" src="/collorg/images/bookmark.svg">. Inverser la sélection Se désinscrire Visibilité Attention !

Une erreur s'est produite. Si vous étiez en train de sauvegarder
un document, veuillez vérifier que tous les champs obligatoires ont
été remplis. Attention ! L'attachement de fichiers ne fonctionne que pour les navigateurs suivants Attention! Ce message est privé. Seuls ses membres y auront accès sur le site. Mauvais mot de passe Vous n'avez créé aucun groupe personnel. Vous allez recevoir un mail avec le fichier iCalendar attaché. Vos groupes ou unités Votre mot de passe a été modifié Votre mot de passe est géré par un annuaire LDAP. Il ne peut être modifié par cette application. droit d'attacher un fichier encore un effort par par mot clef par utilisateur vérification séparateur: virgule connexion créer le groupe créé édition emails, séparés par des virgules de nom du groupe nom du groupe ou d'un membre du groupe dans manquant dernière modification  identifiant déconnexion gérer les membres du groupe moyenne nouveau mot de passe prévisualisation privé intranet document protégé public accès en lecture seule rafraîchir version supprimer de cette liste requis rechercher résultat de la recherche sélectionnez une entrée bonne très bonne à nom de la personne très faible faible accès en écriture votre page d'accueil 