#!/usr/bin/env python
#-*- coding:  utf-8 -*-

from random import randint
from unittest import TestCase
from .. import cog_table

class Test(TestCase):
    def reset(self):
        self.universe = cog_table('collorg.core.base_table')
        self.set_1 = self.universe()
        self.comp_set_1 = self.universe()
        self.set_2 = self.universe()
        self.subset_1_2 = self.universe()
        self.set_3 = self.universe()
        self.comp_set_1 = self.universe()
        self.empty_set = self.universe()
        self.user = cog_table('collorg.actor.user')
        self.user_access_data = self.user._rev_access_._data_
        self.blog_post = cog_table('collorg.communication.blog.post')

    def setUp(self):
        hexchars = '0123456789abcdef'
        self.reset()
        self.c1 = hexchars[randint(0,15)]
        self.c2 = hexchars[randint(0,15)]
        self.c3 = hexchars[randint(0,15)]
        #XXX WARNING! The universe must be defined by a constraint...
        self.universe.cog_oid_.set_not_null()
        #XXX ... Otherwise, the SQL is buggy.
        self.set_1.cog_oid_.value = ('{}%'.format(self.c1), 'like')
        self.comp_set_1.cog_oid_.value = ('{}%'.format(self.c1), 'not like')
        self.set_2.cog_oid_.value = ('_{}%'.format(self.c2), 'like')
        self.subset_1_2.cog_oid_.value = (
            '{}{}%'.format(self.c1, self.c2), 'like')
        self.set_3.cog_oid_.value = ('__{}%'.format(self.c3), 'like')
        self.empty_set.cog_oid_.value = 'X'

    def __check__(self, left, right):
        print(left.select(just_return_sql=True))
        print(left.count())
        print(right.select(just_return_sql=True))
        print(right.count())

    def __check__eq(self, left, right):
        self.__check__(left, right)
        self.assertTrue(left == right)
        self.assertEqual(left.count(), right.count())

    def __check__ne(self, left, right):
        self.__check__(left, right)
        self.assertTrue(left != right)
        #self.assertTrue(left.count() != right.count())

    def __check__in(self, left, right):
        self.__check__(left, right)
        self.assertTrue(left in right)
        self.assertLessEqual(left.count(), right.count())

    def __check__lt(self, left, right):
        self.__check__(left, right)
        self.assertTrue(left < right)
        self.assertLess(left.count(), right.count())

    def __check__gt(self, left, right):
        self.__check__(left, right)
        self.assertTrue(left > right)
        self.assertGreater(left.count(), right.count())

    def __check__le(self, left, right):
        self.__check__(left, right)
        self.assertTrue(left <= right)
        self.assertLessEqual(left.count(), right.count())

    def __check__ge(self, left, right):
        self.__check__(left, right)
        self.assertTrue(left >= right)
        self.assertGreaterEqual(left.count(), right.count())

    def limit_test(self):
        rd = randint(1, self.universe.count())
        a = self.universe
        a.cog_limit(rd)
        a.cog_light = True
        self.assertEqual(
            len([elt.cog_oid_ for elt in a.select(fields=(a.cog_oid_,))]),
            rd)

    def list_value_test(self):
        """
        A field value can be assigned a list
        """
        two = self.universe
        two.cog_limit(2)
        a = self.universe
        a.cog_light = True
        a.cog_oid_.value = [elt.cog_oid_ for elt in two]
        self.assertTrue(two.count() == 2)
        self.__check__eq(a, two)

    def minus_field_test(self):
        ds = self.universe
        hexa_d = "0123456789"
        for c in hexa_d:
            ds.cog_oid_ -= '{}%'.format(c), 'like'
        ls = self.universe
        hexa_l = "abcdef"
        for c in hexa_l:
            ls.cog_oid_ -= '{}%'.format(c), 'like'
        self.__check__eq(ds * ls, self.empty_set)
        self.__check__eq(ds + ls, self.universe)

    def fk_test(self):
        self.__check__le(self.user_access_data, self.universe)

    def fk_test_2(self):
        uad = self.user_access_data
        bp = self.blog_post
        uad.cog_fqtn_.value = bp.fqtn
        self.__check__le(uad, bp)
