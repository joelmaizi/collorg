#!/usr/bin/env python
#-*- coding:  utf-8 -*-

from random import randint
from unittest import TestCase
from .. import cog_table

class Test(TestCase):
    def reset(self):
        self.universe = cog_table('collorg.core.base_table')
        self.set_1 = self.universe()
        self.comp_set_1 = self.universe()
        self.set_2 = self.universe()
        self.subset_1_2 = self.universe()
        self.set_3 = self.universe()
        self.comp_set_1 = self.universe()
        self.empty_set = self.universe()

    def setUp(self):
        hexchars = '0123456789abcdef'
        self.reset()
        self.c1 = hexchars[randint(0,15)]
        self.c2 = hexchars[randint(0,15)]
        self.c3 = hexchars[randint(0,15)]
        #XXX WARNING! The universe must be defined by a constraint...
        self.universe.cog_oid_.set_not_null()
        #XXX ... Otherwise, the SQL is buggy.
        self.set_1.cog_oid_.value = ('{}%'.format(self.c1), 'like')
        self.comp_set_1.cog_oid_.value = ('{}%'.format(self.c1), 'not like')
        self.set_2.cog_oid_.value = ('_{}%'.format(self.c2), 'like')
        self.subset_1_2.cog_oid_.value = (
            '{}{}%'.format(self.c1, self.c2), 'like')
        self.set_3.cog_oid_.value = ('__{}%'.format(self.c3), 'like')
        self.empty_set.cog_oid_.value = 'X'

    def a_test(self):
        """
        Field values can be assigned a field. The field can be from
        another table. The value takes the intention of the other field.
        The fields must be of the same type or have compatible types.
        """
        x = self.set_1
        y = cog_table('collorg.core.oid_table')
        y.cog_oid_.value = x.cog_oid_
        self.assertEqual(x.count(), y.count())

    def b_test(self):
        x = self.set_1
        y = -x
        z = cog_table('collorg.core.oid_table')
        z.cog_oid_.value = (x * y).cog_oid_
        self.assertEqual(z.count(), 0)

