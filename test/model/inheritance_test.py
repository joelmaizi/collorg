#!/usr/bin/env python
#-*- coding:  utf-8 -*-

import sys
from random import randint
from unittest import TestCase
from .. import cog_table, output

class Test(TestCase):
    def reset(self):
        pass

    def setUp(self):
        self.namespace = cog_table('collorg.core.namespace')
        self.data_type = cog_table('collorg.core.data_type')

    def ns_test_1(self):
        """Checks presence of oid_table"""
        ok = True
        for ns in self.namespace:
            if ns.name_.value.find('.view') != -1:
                continue
            dt = self.data_type()
            dt.fqtn_.value = '{}.oid_table'.format(ns.name_)
            try:
                self.assertTrue(dt.count() == 1)
            except:
                ok = False
                sys.stderr.write("--Missing oid_table in {} namespace\n".format(
                ns.name_
                ))
        self.assertTrue(ok == True)

    def ___base_table_test_1(self):
        raise NotImplementedError
        """Checks if all tuples in base_table are in oid_table"""
        ok = True
        for ns in self.namespace:
            bt = "{}.base_table".format(ns.name_)
            self.data_type.fqtn_.value = bt
            if self.data_type.exists():
                base_table = self.data_type.db.table(bt)
                oid_table = self.data_type.db.table('{}.oid_table'.format(
                ns.name_
                ))
                try:
                    self.assertTrue(base_table.count()==oid_table.count())
                except:
                    ok = False
                    sys.stderr.write("{} count != oid_table\n".format(bt))
        self.assertTrue(ok==True)
