#!/usr/bin/env python
#-*- coding:  utf-8 -*-

import os
import sys
from random import randint
from unittest import TestCase
from .. import cog_table, cog_kern_table, output

class Test(TestCase):
    def reset(self):
        pass

    def setUp(self):
        self.namespace = cog_table('collorg.core.namespace')
        self.data_type = cog_table('collorg.core.data_type')
        self.field = cog_table('collorg.core.field')
        self.kern_namespace = cog_kern_table('collorg.core.namespace')
        self.kern_data_type = cog_kern_table('collorg.core.data_type')
        self.kern_field = cog_kern_table('collorg.core.field')

    def namespace_sync_test_1(self):
        """namespace kernel vs namespace application"""
        ok = True
        for nsk in self.kern_namespace:
            self.namespace.name_.value = nsk.name_.value
            try:
                self.assertTrue(self.namespace.count()==1)
            except:
                ok = False
                sys.stderr.write("--Missing namespace {} in application\n".format(
                nsk.name_.value
                ))
        self.assertTrue(ok==True)


    def namespace_sync_test_2(self):
        """namespace application vs namespace kernel"""
        ok = True
        for ns in self.namespace:
            if ns.name_.value.find('collorg.') != 0:
                continue
            self.kern_namespace.name_.value = ns.name_.value
            try:
                self.assertTrue(self.kern_namespace.count()==1)
            except:
                ok = False
                sys.stderr.write("--Namespace {} not in kernel\n".format(
                ns.name_.value
                ))
        self.assertTrue(ok==True)

    def data_type_sync_test_1(self):
        """data_type kernel vs data_type application"""
        ok = True
        for dtk in self.kern_data_type:
            self.data_type.fqtn_.value = dtk.fqtn_.value
            try:
                self.assertTrue(self.data_type.count()==1)
            except:
                ok = False
                sys.stderr.write("--Missing data_type {} in application\n".format(
                dtk.fqtn_.value
                ))
        self.assertTrue(ok==True)


    def data_type_sync_test_2(self):
        """data_type application vs data_type kernel"""
        ok = True
        for dt in self.data_type:
            if dt.fqtn_.value.find('collorg.') != 0:
                continue
            self.kern_data_type.fqtn_.value = dt.fqtn_.value
            try:
                self.assertTrue(self.kern_data_type.count()==1)
            except:
                ok = False
                sys.stderr.write("--Data type {} not in kernel\n".format(
                dt.fqtn_.value
                ))
        self.assertTrue(ok==True)

    def field_sync_test_1(self):
        """field kernel vs field application"""
        ok = True
        for fldk in self.kern_field:
            self.field.fqfn_.value = fldk.fqfn_.value
            try:
                self.assertTrue(self.field.count()==1)
            except:
                ok = False
                sys.stderr.write("--Missing field {} in application\n".format(
                fldk.fqfn_.value
                ))
        self.assertTrue(ok==True)


    def field_sync_test_2(self):
        """field application vs field kernel"""
        ok = True
        for fld in self.field:
            if fld.fqfn_.value.find('collorg.') != 0:
                continue
            self.kern_field.fqfn_.value = fld.fqfn_.value
            try:
                self.assertTrue(self.kern_field.count()==1)
            except:
                ok = False
                sys.stderr.write("--Field {} not in kernel\n".format(
                fld.fqfn_.value
                ))
        self.assertTrue(ok==True)
