#!/usr/bin/env python
#-*- coding:  utf-8 -*-

import sys
from random import randint
from unittest import TestCase
from .. import cog_table, cog_kern_table, output

class Test(TestCase):
    def reset(self):
        pass

    def setUp(self):
        self.data_type = cog_table('collorg.core.data_type')
        self.field = cog_table('collorg.core.field')

    def field_test(self):
        """Checks if all fields of a table are in 'collorg.core.field'
        """
        ok = True
        for dt in self.data_type:
            table = self.data_type.db.table(dt.fqtn_.value)
            for field in table._cog_fields:
                self.field.fqfn_.value = field.fqfn
                try:
                    self.assertTrue(self.field.count()==1)
                except:
                    ok = False
                    sys.stderr.write(
                        "--Missing field {} in 'collorg.core.field'\n".format(
                        field.fqfn
                        ))
        self.assertTrue(ok==True)
