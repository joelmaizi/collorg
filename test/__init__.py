#!/usr/bin/env python

from collorg.controller.controller import Controller

cog_ctrl = Controller()
cog_db = cog_ctrl.db
cog_table = cog_db.table
cog_kern_ctrl = Controller('collorg_db')
cog_kern_db = cog_kern_ctrl.db
cog_kern_table = cog_kern_db.table
output = open("/tmp/collorg_test_errors.sql", "w")
