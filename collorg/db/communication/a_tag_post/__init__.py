#-*- coding: utf-8 -*-

from collorg.orm.table import Table

class A_tag_post(Table):
    #>>> AUTO_COG REL_PART. DO NOT EDIT!
    _cog_schemaname = 'collorg.communication'
    _cog_tablename = 'a_tag_post'
    _cog_templates_loaded = False

    from .cog import relational as cog_r
    # DIRECT
    _data_type_ = cog_r._data_type_
    _tag_ = cog_r._tag_
    _post_ = cog_r._post_
    #<<< AUTO_COG REL_PART. Your code goes after
    def __init__(self, db, **kwargs):
        #>>> AUTO_COG DOC. DO NOT EDIT
        """
        * _db : ref. to database. usage: self.db.table(fqtn)
        fields list:
        * tag_ : string, PK, not null, FK
        * post_ : c_oid, PK, not null, FK
        * order_ : int4
        * data_type_ : c_fqtn, FK
        * status_ : string
        * inst_tag_ : bool
        """
        #<<< AUTO_COG DOC. Your code goes after
        super(A_tag_post, self).__init__(db, **kwargs)

