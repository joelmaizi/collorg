#-*- coding: UTF-8 -*-

from collorg.orm.table import Table

class File(Table):
    #>>> AUTO_COG REL_PART. DO NOT EDIT!
    _cog_schemaname = 'collorg.communication.view'
    _cog_tablename = 'file'
    _cog_templates_loaded = False

    #<<< AUTO_COG REL_PART. Your code goes after
    def __init__(self, db, **kwargs):
        #>>> AUTO_COG DOC. DO NOT EDIT
        """
        * _db : ref. to database. usage: self.db.table(fqtn)
        fields list:
        * cog_oid_ : c_oid
        * cog_creat_date_ : timestamp
        * name_ : string
        * signature_ : string
        * visibility_ : string
        * data_oid_ : c_oid
        * data_fqtn_ : c_fqtn
        * data_creat_date_ : timestamp
        * data_title_ : string
        * data_introductory_paragraph_ : string
        * data_text_ : wiki
        * data_author_oid_ : c_oid
        """
        #<<< AUTO_COG DOC. Your code goes after
        super(File, self).__init__(db, **kwargs)

