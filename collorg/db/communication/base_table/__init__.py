#-*- coding: utf-8 -*-

import json
from collections import OrderedDict
from collorg.db.core.base_table import Base_table as CoreBT
from collorg.db.communication.oid_table import Oid_table

class Base_table(CoreBT, Oid_table):
    #>>> AUTO_COG REL_PART. DO NOT EDIT!
    _cog_schemaname = 'collorg.communication'
    _cog_tablename = 'base_table'
    _cog_templates_loaded = False

    #<<< AUTO_COG REL_PART. Your code goes after
    _is_cog_post = False
    _is_cog_event = False
    _is_cog_folder = False
    def __init__(self, db, **kwargs):
        #>>> AUTO_COG DOC. DO NOT EDIT
        """
        * _db : ref. to database. usage: self.db.table(fqtn)
        fields list:
        * cog_oid_ : c_oid, uniq, not null
        * cog_fqtn_ : c_fqtn, not null
        * cog_signature_ : text, inherited
        * cog_test_ : bool, inherited
        * cog_creat_date_ : timestamp, inherited
        * cog_modif_date_ : timestamp, inherited
        * cog_environment_ : c_oid, inherited
        * cog_state_ : text, inherited
        * title_ : string
        * introductory_paragraph_ : string
        * text_ : wiki
        * author_ : c_oid
        * visibility_ : string
        """
        #<<< AUTO_COG DOC. Your code goes after
        super(Base_table, self).__init__(db, **kwargs)

    def get_visible(self, user):
        """
        self represents a set of posts. visible is the subset of posts
        accessible to the user.
        """
        visible = self()
        visible.cog_oid_.value = self.cog_oid_
        if user and user.has_access(self):
            pass
        elif not user or user.alien_.value is True:
            visible.visibility_.value = 'public'
        else:
            visible.visibility_.value = 'private', '!='
        return visible

    def __cog_json_handler(self, obj):
        if hasattr(obj, 'isoformat'):
            return obj.isoformat()
        else:
            raise TypeError('''Object of type {} with value of {}'''
                ''' is not JSON serializable''').format(type(obj), repr(obj))

    def _cog_json_dumps(self):
        """returns a json.dumps of self.__extension"""
        l_out = []
        self.visibility_.value = 'public'
        for elt in self:
            out = OrderedDict()
            for field in elt._cog_fields:
                out[field.name] = field.value
            l_out.append(out)
        return json.dumps(l_out, default=self.__cog_json_handler)

    def _cog_json_loads(self, data):
        """loads data into a Relation object"""
        new = self()
        in_ = json.loads(data)
        for elt in in_:
            assert elt['cog_fqtn'] == self.fqtn
        new.__extension = in_
        return new
