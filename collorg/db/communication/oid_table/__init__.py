#-*- coding: utf-8 -*-

from collorg.orm.table import Table

class Oid_table(Table):
    #>>> AUTO_COG REL_PART. DO NOT EDIT!
    _cog_schemaname = 'collorg.communication'
    _cog_tablename = 'oid_table'
    _cog_templates_loaded = False

    from .cog import relational as cog_r
    # REVERSE
    _rev_allowed_post_type_ = cog_r._rev_allowed_post_type_
    _rev_user_check_ = cog_r._rev_user_check_
    _rev_a_tag_post_ = cog_r._rev_a_tag_post_
    _rev_bookmark_ = cog_r._rev_bookmark_
    _rev_attachment_ = cog_r._rev_attachment_
    _rev_a_post_data_post_ = cog_r._rev_a_post_data_post_
    _rev_a_post_data_data_ = cog_r._rev_a_post_data_data_
    _rev_comment_ = cog_r._rev_comment_
    #<<< AUTO_COG REL_PART. Your code goes after
    def __init__(self, db, **kwargs):
        #>>> AUTO_COG DOC. DO NOT EDIT
        """
        * _db : ref. to database. usage: self.db.table(fqtn)
        fields list:
        * cog_oid_ : c_oid, PK, uniq, not null
        * cog_fqtn_ : c_fqtn, PK, not null
        """
        #<<< AUTO_COG DOC. Your code goes after
        super(Oid_table, self).__init__(db, **kwargs)

    def _wipe_cache(self):
        for obj in self:
            if obj.__class__ == Oid_table:
                obj = obj.get()
            if obj._is_cog_post:
                return obj._wipe_cache()

