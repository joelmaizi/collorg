# DIRECT
def _get_allowed_type(self):
    allowed_type_ = self.db.table('collorg.core.data_type')
    allowed_type_.fqtn_.value = self.allowed_type_
    return allowed_type_
def _set_allowed_type(self, allowed_type_):
    self.allowed_type_.value = allowed_type_.fqtn_

_allowed_type_ = property(
    _get_allowed_type, _set_allowed_type)

def _get_post(self):
    post_ = self.db.table('collorg.communication.oid_table')
    post_.cog_oid_.value = self.post_
    return post_
def _set_post(self, post_):
    self.post_.value = post_.cog_oid_

_post_ = property(
    _get_post, _set_post)

