#-*- coding: utf-8 -*-

from collorg.db.core.base_table import Base_table

class Allowed_post_type(Base_table):
    #>>> AUTO_COG REL_PART. DO NOT EDIT!
    _cog_schemaname = 'collorg.communication'
    _cog_tablename = 'allowed_post_type'
    _cog_templates_loaded = False

    from .cog import relational as cog_r
    # DIRECT
    _allowed_type_ = cog_r._allowed_type_
    _post_ = cog_r._post_
    #<<< AUTO_COG REL_PART. Your code goes after
    def __init__(self, db, **kwargs):
        #>>> AUTO_COG DOC. DO NOT EDIT
        """
        * _db : ref. to database. usage: self.db.table(fqtn)
        fields list:
        * cog_oid_ : c_oid, uniq, not null
        * cog_fqtn_ : c_fqtn, not null
        * cog_signature_ : text, inherited
        * cog_test_ : bool, inherited
        * cog_creat_date_ : timestamp, inherited
        * cog_modif_date_ : timestamp, inherited
        * cog_environment_ : c_oid, inherited
        * cog_state_ : text, inherited
        * allowed_type_ : text, PK, not null, FK
        * post_ : c_oid, PK, not null, FK
        """
        #<<< AUTO_COG DOC. Your code goes after
        super(Allowed_post_type, self).__init__(db, **kwargs)

