#-*- coding: utf-8 -*-

from collorg.orm.table import Table

class Oid_table(Table):
    #>>> AUTO_COG REL_PART. DO NOT EDIT!
    _cog_schemaname = 'collorg.location'
    _cog_tablename = 'oid_table'
    _cog_templates_loaded = False

    #<<< AUTO_COG REL_PART. Your code goes after
    def __init__(self, db, **kwargs):
        #>>> AUTO_COG DOC. DO NOT EDIT
        """
        * _db : ref. to database. usage: self.db.table(fqtn)
        fields list:
        * cog_oid_ : c_oid, PK, uniq, not null
        * cog_fqtn_ : c_fqtn, PK, not null
        """
        #<<< AUTO_COG DOC. Your code goes after
        super(Oid_table, self).__init__(db, **kwargs)

