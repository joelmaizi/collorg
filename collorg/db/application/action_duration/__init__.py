#-*- coding: UTF-8 -*-

from collorg.orm.table import Table

class Action_duration(Table):
    #>>> AUTO_COG REL_PART. DO NOT EDIT!
    _cog_schemaname = 'collorg.application'
    _cog_tablename = 'action_duration'
    _cog_templates_loaded = False

    from .cog import relational as cog_r
    # DIRECT
    _action_ = cog_r._action_
    #<<< AUTO_COG REL_PART. Your code goes after
    def __init__(self, db, **kwargs):
        #>>> AUTO_COG DOC. DO NOT EDIT
        """
        * _db : ref. to database. usage: self.db.table(fqtn)
        fields list:
        * begin_ : timestamp, PK, not null
        * end_ : timestamp, PK, not null
        * action_ : c_oid, PK, not null, FK
        """
        #<<< AUTO_COG DOC. Your code goes after
        super(Action_duration, self).__init__(db, **kwargs)

    def contains(self, date):
        return self.begin_.value < date and date < self.end_.date

    def current(self, date):
        cur = self()
        cur._action_ = self._action_
        cur.begin_.value = date, '<'
        cur.end_.value = date, '>'
        if cur.exists():
            return cur.get()
        return None

    def previous(self, date):
        prev = self()
        prev._action_ = self._action_
        prev.order_by(prev.end_)
        prev.end_.value = date, '<'
        prev.end_.set_descending_order()
        if prev.exists():
            prev.cog_limit(1)
            return prev.select()[0]
        return None

    def next_(self, date):
        next_ = self()
        next_._action_ = self._action_
        next_.order_by(self.begin_)
        next_.begin_.value = date, '>'
        if next_.exists():
            next_.cog_limit(1)
            return next_.select()[0]
        return None
