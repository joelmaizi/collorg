# DIRECT
def _get_action(self):
    action_ = self.db.table('collorg.application.action')
    action_.cog_oid_.value = self.action_
    return action_
def _set_action(self, action_):
    self.action_.value = action_.cog_oid_

_action_ = property(
    _get_action, _set_action)

