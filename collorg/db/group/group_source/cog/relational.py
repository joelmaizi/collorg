# DIRECT
def _get_contact(self):
    contact_ = self.db.table('collorg.actor.user')
    contact_.cog_oid_.value = self.contact_
    return contact_
def _set_contact(self, contact_):
    self.contact_.value = contact_.cog_oid_

_contact_ = property(
    _get_contact, _set_contact)

# REVERSE
@property
def _rev_group_(self):
    elt = self.db.table('collorg.group.group')
    elt._group_source_ = self
    if 'cog_oid_' in self.__dict__ and self.cog_oid_.value:
        if not '_cog_direct_refs' in elt.__dict__:
            elt._cog_direct_refs = []
        elt._cog_direct_refs.append(self.cog_oid_.value)
    return elt

