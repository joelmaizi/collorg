#-*- coding: UTF-8 -*-

from collorg.db.communication.base_table import Base_table

class Group_source(Base_table):
    #>>> AUTO_COG REL_PART. DO NOT EDIT!
    _cog_schemaname = 'collorg.group'
    _cog_tablename = 'group_source'
    _cog_templates_loaded = False

    from .cog import relational as cog_r
    # DIRECT
    _contact_ = cog_r._contact_
    # REVERSE
    _rev_group_ = cog_r._rev_group_
    #<<< AUTO_COG REL_PART. Your code goes after
    def __init__(self, db, **kwargs):
        #>>> AUTO_COG DOC. DO NOT EDIT
        """
        * _db : ref. to database. usage: self.db.table(fqtn)
        fields list:
        * cog_oid_ : c_oid, uniq, not null
        * cog_fqtn_ : c_fqtn, not null
        * cog_signature_ : text, inherited
        * cog_test_ : bool, inherited
        * cog_creat_date_ : timestamp, inherited
        * cog_modif_date_ : timestamp, inherited
        * cog_environment_ : c_oid, inherited
        * cog_state_ : text, inherited
        * title_ : string, inherited
        * introductory_paragraph_ : string, inherited
        * text_ : wiki, inherited
        * author_ : c_oid, inherited
        * visibility_ : string, inherited
        * name_ : string, PK, not null
        * contact_ : c_oid, FK
        """
        #<<< AUTO_COG DOC. Your code goes after
        super(Group_source, self).__init__(db, **kwargs)

