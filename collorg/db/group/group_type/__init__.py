#-*- coding: UTF-8 -*-

from collorg.orm.table import Table

class Group_type(Table):
    #>>> AUTO_COG REL_PART. DO NOT EDIT!
    _cog_schemaname = 'collorg.group'
    _cog_tablename = 'group_type'
    _cog_templates_loaded = False

    from .cog import relational as cog_r
    # REVERSE
    _rev_group_ = cog_r._rev_group_
    #<<< AUTO_COG REL_PART. Your code goes after
    def __init__(self, db, **kwargs):
        #>>> AUTO_COG DOC. DO NOT EDIT
        """
        * _db : ref. to database. usage: self.db.table(fqtn)
        fields list:
        * name_ : string, PK, not null
        * description_ : wiki
        """
        #<<< AUTO_COG DOC. Your code goes after
        super(Group_type, self).__init__(db, **kwargs)

