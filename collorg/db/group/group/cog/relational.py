# DIRECT
def _get_data(self):
    data_ = self.db.table('collorg.core.oid_table')
    data_.cog_oid_.value = self.data_
    return data_
def _set_data(self, data_):
    self.data_.value = data_.cog_oid_

_data_ = property(
    _get_data, _set_data)

def _get_group_source(self):
    group_source_ = self.db.table('collorg.group.group_source')
    group_source_.name_.value = self.group_source_
    return group_source_
def _set_group_source(self, group_source_):
    self.group_source_.value = group_source_.name_

_group_source_ = property(
    _get_group_source, _set_group_source)

def _get_group_type(self):
    group_type_ = self.db.table('collorg.group.group_type')
    group_type_.name_.value = self.group_type_
    return group_type_
def _set_group_type(self, group_type_):
    self.group_type_.value = group_type_.name_

_group_type_ = property(
    _get_group_type, _set_group_type)

# REVERSE
@property
def _rev_definition_(self):
    elt = self.db.table('collorg.group.definition')
    elt._group_ = self
    if 'cog_oid_' in self.__dict__ and self.cog_oid_.value:
        if not '_cog_direct_refs' in elt.__dict__:
            elt._cog_direct_refs = []
        elt._cog_direct_refs.append(self.cog_oid_.value)
    return elt

