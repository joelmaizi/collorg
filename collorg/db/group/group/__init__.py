#-*- coding: utf-8 -*-

from collorg.utils.ical import ICal
from collorg.db.communication.blog.post import Post

class Group(Post):
    #>>> AUTO_COG REL_PART. DO NOT EDIT!
    _cog_schemaname = 'collorg.group'
    _cog_tablename = 'group'
    _cog_templates_loaded = False

    from .cog import relational as cog_r
    # DIRECT
    _data_ = cog_r._data_
    _group_source_ = cog_r._group_source_
    _group_type_ = cog_r._group_type_
    # REVERSE
    _rev_definition_ = cog_r._rev_definition_
    #<<< AUTO_COG REL_PART. Your code goes after
    no_attach = True
    _is_cog_group = True
    __cog_type_name = 'Group'
    """
    A group is one of the two elements by which an access is granted
    to a user (collorg.actor.user). The other element is the role (collorg.actor.role).
    Five roles exist by default (see collorg.actor.role).
    By default, a group is created for each unit in the database to it (see
    collorg.organization.unit)
    """
    def __init__( self, db, **kwargs ):
        #>>> AUTO_COG DOC. DO NOT EDIT
        """
        * _db : ref. to database. usage: self.db.table(fqtn)
        fields list:
        * cog_oid_ : c_oid, uniq, not null
        * cog_fqtn_ : c_fqtn, not null
        * cog_signature_ : text, inherited
        * cog_test_ : bool, inherited
        * cog_creat_date_ : timestamp, inherited
        * cog_modif_date_ : timestamp, inherited
        * cog_environment_ : c_oid, inherited
        * cog_state_ : text, inherited
        * title_ : string, inherited, not null
        * introductory_paragraph_ : string, inherited
        * text_ : wiki, inherited, not null
        * author_ : c_oid, inherited, not null
        * visibility_ : string, inherited, not null
        * public_ : bool, inherited
        * comment_ : bool, inherited
        * expiry_date_ : timestamp, inherited
        * important_ : bool, inherited
        * broadcast_ : bool, inherited
        * name_ : string, PK, not null
        * data_ : c_oid, PK, not null, FK
        * open_ : bool
        * group_type_ : string, FK
        * group_source_ : string, FK
        """
        #<<< AUTO_COG DOC. Your code goes after
        super( Group, self ).__init__( db, **kwargs )


    @property
    def _cog_web_label(self):
        return ["""<img src="/collorg/images/group.svg">"""]
    @property
    def _cog_label(self):
        return ['<img src="/collorg/images/group.svg" class="medicon"> {} {}',
            self.group_type_, self.title_]

    def has_access(self, data, write=None, attach=None):
        ga = self.db.table('collorg.access.group_access')
        ga._accessed_data_ = data
        ga._group_data_ = self
        ga.write_.value = write
        #TODO add attach attribute to group_access
        return ga.exists()

    def __set_group_access(self, data, write=False, attach=False):
        assert self.count() == 1 and data.count() == 1
        ga = self.db.table('collorg.access.group_access')
        ga._accessed_data_ = data
        ga._group_data_ = self
        ga.write_.value = write
        return ga

    def grant_access(self, data, write=False, attach=False):
        ga = self.__set_group_access(data, write=write, attach=attach)
        if ga.is_empty():
            ga.insert()
            if data._is_cog_event:
                self.__update_ical()

    def revoke_access(self, data):
        ga = self.__set_group_access(data, write=None, attach=None)
        if not ga.is_empty():
            ga.delete()
            if data._is_cog_event:
                self.__update_ical()

    def grant_attach_access(self, data):
        ga = self.__set_group_access(data, write=None, attach=False)
        nga = ga()
        nga.attach_.value = True
        if not ga.is_empty():
            ga.update(nga)

    def revoke_attach_access(self, data):
        ga = self.__set_group_access(data, write=None, attach=True)
        nga = ga()
        nga.attach_.value = False
        if not ga.is_empty():
            ga.update(nga)

    def grant_write_access(self, data):
        ga = self.__set_group_access(data, write=False, attach=None)
        nga = ga()
        nga.write_.value = True
        if not ga.is_empty():
            ga.update(nga)

    def revoke_write_access(self, data):
        ga = self.__set_group_access(data, write=True, attach=None)
        nga = ga()
        nga.write_.value = False
        if not ga.is_empty():
            ga.update(nga)

    def root_topic(self):
        topic = self.db.table('collorg.web.topic')
        topic.cog_environment_.value = self.cog_oid_.value
        #topic.path_info_.value = ''
        return topic

    def add_see_also(self, data):
        apd = self._rev_a_post_data_data_
        apd._post_ = data
        if apd.is_empty():
            apd.see_also_.value = False
            apd.insert()
            self.grant_access(data)

    @property
    def events(self):
        events = self.db.table('collorg.event.event')
        events.cog_oid_.value = \
            self._rev_group_access_group_data_._accessed_data_.cog_oid_
        return events

    @property
    def posts(self):
        posts = self.db.table('collorg.communication.blog.post')
        posts.cog_oid_.value = \
            self._rev_group_access_group_data_._accessed_data_.cog_oid_
        return posts

    @property
    def ical(self):
        return ICal(self.events, self)

    def __update_ical(self):
        ICal(self.events, self).store()
