#-*- coding: utf-8 -*-

import sys
from collorg.orm.table import Table

if sys.version_info.major < 3:
    input = raw_input

class Lost_found(Table):
    #>>> AUTO_COG REL_PART. DO NOT EDIT!
    _cog_schemaname = 'collorg.core.view'
    _cog_tablename = 'lost_found'
    _cog_templates_loaded = False

    #<<< AUTO_COG REL_PART. Your code goes after
    def __init__(self, db, **kwargs):
        #>>> AUTO_COG DOC. DO NOT EDIT
        """
        * _db : ref. to database. usage: self.db.table(fqtn)
        fields list:
        * cog_oid_ : c_oid
        * cog_fqtn_ : c_fqtn
        * bt_cog_oid_ : c_oid
        """
        #<<< AUTO_COG DOC. Your code goes after
        super(Lost_found, self).__init__(db, **kwargs)

    def check(self):
        self.bt_cog_oid_.set_null()
        for elt in self:
            print("missing: {} {}".format(self.cog_oid_, self.cog_fqtn_))

    def clean(self, interactive=True):
        total = self.count()
        self.bt_cog_oid_.set_null()
        to_remove = self.count()
        if not to_remove:
            print("The database is clean.")
            return
        print("About to remove {} elements from {}.".format(
            to_remove, total))
        ok = input("Proceed [y/N]? ")
        if ok.upper() == 'Y':
            oid_table = self.db.table('collorg.core.oid_table')
            oid_table.cog_oid_.value = self.cog_oid_
            for elt in oid_table:
                try:
                    elt.delete()
                except Exception as err:
                    self.db.transaction_begin()
                    for relation, count in self.db.search(elt.cog_oid_.value):
                        names = []
                        for elt in relation:
                            if relation._cog_is_view:
                                print("{} is a view".format(relation.fqtn))
                                continue
                            elt.delete()
                            for field in elt._cog_fields:
                                if field.value == elt.cog_oid_.value:
                                    names.append(field.name)
                        print("{} : {} {}".format(
                            relation.fqtn, count, ", ".join(names)))
                    ok = input("Proceed [y/N]? ")
                    if ok.upper() == 'Y':
                        self.db.commit()
                    else:
                        self.db.rollback()
        total = self.count()
        self.bt_cog_oid_.set_null()
        to_remove = self.count()
        if to_remove:
            print("{} elements are still in lost+found. Rerun clean.".format(
                to_remove))
