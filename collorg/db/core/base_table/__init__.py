#-*- coding: utf-8 -*-
### Copyright © 2011,2014 Joël Maïzi <joel.maizi@lirmm.fr>
### This file is part of collorg

### collorg is free software: you can redistribute it and/or modify
### it under the terms of the GNU General Public License as published by
### the Free Software Foundation, either version 3 of the License, or
### (at your option) any later version.

### This program is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
### GNU General Public License for more details.

### You should have received a copy of the GNU General Public License
### along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import shutil

from collorg.db.core.oid_table import Oid_table
from collorg.orm.table import Table as TClass

class Base_table(Oid_table):
    #>>> AUTO_COG REL_PART. DO NOT EDIT!
    _cog_schemaname = 'collorg.core'
    _cog_tablename = 'base_table'
    _cog_templates_loaded = False

    from .cog import relational as cog_r
    # DIRECT
    _cog_environment_ = cog_r._cog_environment_
    #<<< AUTO_COG REL_PART. Your code goes after
    _cog_base_table = True
    _cog_abstract_table = True
    _is_cog_post = False
    _is_cog_folder = False
    _is_cog_unit = False
    _is_cog_user = False
    _is_cog_group = False
    d_members = {}
    def __init__( self, db, **kwargs ):
        #>>> AUTO_COG DOC. DO NOT EDIT
        """
        * _db : ref. to database. usage: self.db.table(fqtn)
        fields list:
        * cog_oid_ : c_oid, PK, uniq, not null
        * cog_fqtn_ : c_fqtn, PK, not null
        * cog_signature_ : text
        * cog_test_ : bool
        * cog_creat_date_ : timestamp
        * cog_modif_date_ : timestamp
        * cog_environment_ : c_oid, FK
        * cog_state_ : text
        """
        #<<< AUTO_COG DOC. Your code goes after
        self._cog_order_by = []
        TClass.__init__( self, db, **kwargs )
        self.__cache_path = ''

    def get( self, fields = None, just_return_sql = False, recurse = True,
            reload_ = False):
        if (self.fqtn != 'collorg.core.base_table' and
            self.fqtn.split('.')[-1] != 'oid_table'):
            return TClass.get(
                self,
                fields = fields, just_return_sql = just_return_sql,
                reload_ = reload_, recurse = recurse )
        obj = TClass.get(
            self, fields = fields, just_return_sql = just_return_sql,
            reload_ = reload_)
        if just_return_sql:
            return obj
        return self.db.table(
            self.cog_fqtn_.value, cog_oid_ = self.cog_oid_.value ).get(
                reload_ = reload_)

    @property
    def cog_type(self):
        """
        returns a 'collorg.core.data_type' with fqtn_ = self.cog_fqtn_
        """
        table = self.db.table('collorg.core.data_type')
        table.fqtn_.value = self.fqtn
        return table

    def cog_convert_to_bt(self):
        obt = Base_table(self.db)
        for field in obt._cog_fields:
            obt.__dict__[field.pyname] = self.__dict__[field.pyname]
        return obt

    def cog_path(self, data):
        try:
            return eval("data.{}".format(self._cog_paths[data.fqtn]))
        except KeyError:
            raise RuntimeError("Missing path between {} and {}".format(
                data.fqtn, self.fqtn))

    # RELATIONAL

    @property
    def members(self):
        # copy of core.oid_table.Oid_table.members
        access = self._rev_access_.granted()
        group_access = self.\
            _rev_group_access_accessed_data_._group_data_._rev_access_.granted()
        users = access._user_ + group_access._user_
        if not self._rev_hierarchy_parent_.is_empty():
            for child in self._rev_hierarchy_parent_._child_:
                users += child.members
        return users

    def get_root_topic(self):
        return self.db.table('collorg.web.topic').get_root(self)

    # Cache stuff

    @property
    def _cache_path(self):
        if not self.__cache_path:
            self.__cache_path = '{}/{}/{}/{}'.format(
                self.db._cog_params['cache_path'],
                self.cog_oid_.value[0:2], self.cog_oid_.value[2:4],
                self.cog_oid_)
        return self.__cache_path

    def _cog_cache_link(self, filename):
        return "{}/{}".format(self._cache_path, filename)

    def _wipe_cache(self, deja_vus=None):
        """
        The cache is wiped when there is a modification. It'll be re-generated
        on first anonymous access.
        """
        if deja_vus is None:
            deja_vus = []
        for elt in self:
            cog_oid = elt.cog_oid_.value
            if cog_oid in deja_vus:
                continue
            deja_vus.append(cog_oid)
            if os.path.exists(elt._cache_path):
                shutil.rmtree(elt._cache_path)
            data = self()
            data.cog_oid_.value = \
                elt._rev_a_post_data_post_._data_.cog_oid_
            deja_vu = self()
            deja_vu.cog_oid_.value = deja_vus
            data -= deja_vu
            data._wipe_cache(deja_vus)

    def _cog_write_cache(self, filename, method, **kwargs):
        if not os.path.exists(self._cache_path):
            os.makedirs(self._cache_path)
        file_ = self._cog_cache_link(filename)
        if not os.path.exists(file_):
            open(file_, 'w').write(str(method(**kwargs)))
        return open(file_).read()
