#!/usr/bin/python
#-*- coding: utf-8 -*-

import smtplib

from email.MIMEText import MIMEText
from email.MIMEMultipart import MIMEMultipart
#from email.encoders import encode_quopri

class Mail():
    def __init__(self, db):
        self.db = db
        self.__smtp_server = db._cog_params['smtp_server']
        self.__charset = db._cog_params['charset']
        self.__email = MIMEMultipart()

    def set_from(self, from_):
        self.__from = from_
        self.__email['From'] = from_
        self.__email['Sender'] = from_

    def set_to(self, to):
        self.__to = to
        self.__email['To'] = ",".join(to)

    def set_cc(self, cc_):
        self.__email['Cc'] = ','.join(cc_)

    def set_bcc(self, bcc_):
        self.__email['Bcc'] = ",".join(bcc_)

    def set_subject(self, subject):
        self.__email['Subject'] = unicode(subject, self.__charset)

    def set_body(self, text, _subtype='plain'):
        msg = MIMEText(text, _subtype, self.__charset)
        self.__email.attach(msg)

    def send(self):
        smtp = smtplib.SMTP(self.__smtp_server)
        smtp.sendmail(
            self.__from,
            self.__to,
            self.__email.as_string()
            )
        smtp.quit()

    def attach(self, file_name, text=None):
        if text is None:
            f = file(file_name)
            attachment = MIMEText(f.read(), 'plain', self.__charset)
        else:
            attachment = MIMEText(str(text), 'plain', self.__charset)
        attachment.add_header(
            'Content-Disposition', 'attachment', filename=file_name
            )
        self.__email.attach(attachment)

    def __str__(self):
        return "{}".format(self.__email)
