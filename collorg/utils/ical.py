#!/usr/bin/env python
#-*- coding: utf-8 -*-

import os
import urllib
import pytz
from unidecode import unidecode
from icalendar import Calendar, vText, Event as ICalEvent
from collorg.controller.controller import Controller
from collorg.templates.document_type.html import Html

class ICal(Calendar):
    def __init__(self, events, data, key=None):
        self.__events = events
        assert data.cog_oid_.value
        self.__db = data.db
        self._cog_controller = data.db._cog_controller
        self.__data_oid = data.cog_oid_.value
        self.__key = key
        self.__title = data.title_.value.replace(
            "/", "-").replace(" ", "_")
        self.__timezone = events.db._cog_params['timezone']
        self.__base_dir = None
        super(Calendar, self).__init__()

    def to_ical(self, public=False):
        self.add('prodid', '-//Collorg//www.collorg.org')
        self.add('version', '2.0')
        self.add('calscale', 'GREGORIAN')
        for elt in self.__events:
            if public and elt.visibility_.value != 'public':
                continue
            event = ICalEvent()
            event.add('created', self.__to_utc(elt.cog_creat_date_.value))
            event.add('last-modified', self.__to_utc(
                elt.cog_modif_date_.value))
            event.add('summary', vText(elt.cog_label()))
            event.add('description', vText(elt.text_.value))
            event.add('dtstart', self.__to_utc(elt.begin_date_.value))
            event.add('dtend', self.__to_utc(elt.end_date_.value))
            event.add('dtstamp', self.__to_utc(elt.cog_creat_date_.value))
            event.add('uid', elt.cog_oid_.value)
            event.add('location', elt.location_str_.value)
            event.add('sequence', elt.sequence_.value)
            event.add('url', Html(elt).url())
            self.add_component(event)
        return super(self.__class__, self).to_ical()

    def __to_utc(self, naive_dt):
        """Transforms a "naive datetime" in a UTC datetime.
        Uses the system timezone or the timezone defined in the
        /etc/collorg/<conf> file.
        """
        dt = pytz.timezone(self.__timezone).localize(naive_dt, is_dst=None)
        return pytz.timezone('UTC').localize(
            naive_dt - dt.utcoffset(), is_dst=None)

    def __str__(self):
        return self.to_ical()

    def __rel_path(self):
        assert self.__data_oid
        rel_path = "ical/{}/{}/{}".format(
            self.__data_oid[0:2], self.__data_oid[2:4], self.__data_oid)
        if self.__key:
            rel_path = "{}/{}".format(rel_path, self.__key)
        return rel_path

    def __unaccent(self, string_):
        ctrl = self._cog_controller
        return unidecode(unicode(string_, ctrl.db._cog_params['charset']))

    def __rel_file_path(self):
        return "{}/{}.ics".format(
            self.__rel_path(), self.__unaccent(self.__title))

    def __abs_path(self):
        assert self.__data_oid
        abs_path = "{}/{}".format(
            self.__db._cog_params['document_root'], self.__rel_path())
        if not os.path.exists(abs_path):
            os.makedirs(abs_path)
        return abs_path

    def __abs_file_path(self):
        return "{}/{}.ics".format(
            self.__abs_path(), self.__unaccent(self.__title))

    def url(self):
        ctrl = self._cog_controller
        if not os.path.exists(self.__abs_file_path()):
            self.store()
        link = "{}://{}/{}".format(
            ctrl.__dict__.get('_url_scheme', ctrl.db._cog_params['_url_scheme']),
            ctrl.__dict__.get('_server_name', ctrl.db._cog_params['_server_name']),
            self.__rel_file_path())
        return link.encode(self._cog_controller._charset)

    def store(self):
        """Stores only public events"""
        assert self.__data_oid
        self.__events.visibility_.value = 'public'
        self.__base_dir = "{}/{}".format(
            self.__db._cog_params['document_root'], self.__rel_path)
        afp = self.__abs_file_path()
        if os.path.exists(afp):
            os.unlink(afp)
        open(afp, "w").write(str(self.to_ical(public=True)))
