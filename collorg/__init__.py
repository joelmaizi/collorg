#-*- coding: utf-8 -*-
### Copyright © 2011,2014 Joël Maïzi <joel.maizi@lirmm.fr>
### This file is part of collorg

### collorg is free software: you can redistribute it and/or modify
### it under the terms of the GNU General Public License as published by
### the Free Software Foundation, either version 3 of the License, or
### (at your option) any later version.

### This program is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
### GNU General Public License for more details.

### You should have received a copy of the GNU General Public License
### along with this program.  If not, see <http://www.gnu.org/licenses/>.

__import__('pkg_resources').declare_namespace(__name__)
__doc__ = """
collaborative organization framework

Mainly used in a collorg repository to script as follows:

#!/usr/bin/env python
#-*- coding: utf-8 -*-

from collorg.controller.controller import Controller

db_name = None
# if the script is to be run outside a collorg repository, db_name must be set
# to the database name.

ctrl = Controller(db_name)
model = ctrl.model
relation = model.relation
my_rel = relation('<fully qualified table name>')
# Play with your relation: help(relation) or help(my_rel)
...

see cog command to manage a collorg repository
"""
