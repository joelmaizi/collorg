#!/usr/bin/env python
#-*- coding: utf-8 -*-

### Copyright © 2011,2014 Joël Maïzi <joel.maizi@lirmm.fr>
### This file is part of collorg

### collorg is free software: you can redistribute it and/or modify
### it under the terms of the GNU General Public License as published by
### the Free Software Foundation, either version 3 of the License, or
### (at your option) any later version.

### This program is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
### GNU General Public License for more details.

### You should have received a copy of the GNU General Public License
### along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
collorg "controller"

The controller module provides the Controller class.

Example of a script instanciating a Controller:

from collorg.controller.controller import Controller
ctrl = Controller()
# The controller grants access to the model
model = ctrl.model
# The method relation is the way to instanciate a relational object
relation = model.relation

my_rel = relation('<FQTN>')
# FQTN stands for (fully qualified table name), for example :
# * collorg.actor.user
# * collorg.communication.blog.post
"""

import os
import collorg
import datetime
from functools import wraps
from collorg.utils import db_connector
from collorg.orm.db import Db
from collorg.orm.table import Table
import gettext

def _template(func):
    @wraps(func)
    def wrapper(self, **kwargs):
        # kwargs: cog_write, cog_moderate, cog_admin
        kw = kwargs
        begin = datetime.datetime.now()
        ctrl = self._cog_controller
        cog_user = ctrl.user
        if 'no_cog_user' in kwargs:
            cog_user = None
        cog_charset = ctrl._charset
        func_name = func.func_name
        cog_reminder = kw.pop('cog_reminder', "")
        res = []
        trace = "trace"
        if 'cog_first_call' in kwargs and self._is_of_type_post:
            trace = "trace_external"
            if not ctrl.check_visibility(self):
                return self.w3access_denied()
        kw.pop('cog_first_call', None)
        #? wma
        if not ctrl.check(self, func):
            return "<!-- access denied: {}.{} -->".format(
                self.fqtn, func_name)
        if 'cog_alt_target' in kw:
            kw['target'] = kw['cog_alt_target']
        begin_funct = datetime.datetime.now()
        f_res = func(
            self, cog_charset=cog_charset, cog_user=cog_user,
            cog_environment=ctrl.cog_exec_env, **kw).strip()
        duration = datetime.datetime.now() - begin_funct
        if ctrl.cog_dwi and func_name.find('w3debug') == -1 and kw.get('debug', True):
            res.append(self._w3debug(
                trace=trace, f_res=f_res, func=func,
                begin_funct=begin_funct, duration=duration,
                begin=begin))
        else:
            res.append(f_res)
        res.append(cog_reminder.encode(ctrl._charset))
        return "\n".join(res)
    return wrapper

class Controller(object):
    _charset = None
    _debug = False
    __d_inh_tree = {}
    __app_path = None
    _d_actions = {}
    _d_actions_by_oid = {}
    _d_check = {}
    i18n = gettext.translation(
        'messages', '/usr/share/collorg/locale',['en'])
    def __init__(self, config_file=None, verbose=False):
        self._kwargs = {}
        #DEBUG TEMPLATE IN CONSOLE self.user = None
        self._cog_ajax = None
        self._cog_method = None
        self._cog_ref_oid = None
        self._cog_fqtn_ = None
        self._cog_oid_ = None
        self._cog_cmd = None
        self.cog_exec_env = None
        self.__session = None
        self._session_key = None
        self._json_res = {}
        self.__collorg_path = os.path.dirname(collorg.__file__)
        self.__repos_path = None
        self.__now = None
        self.__db_name = config_file
        if self.__db_name is None:
            self.__repos_path, self.__db_name = db_connector.get_cog_infos()
        if self.__repos_path is None and self.__db_name == 'collorg_db':
            self.__repos_path = self.__collorg_path
        self.db = Db(
            self, *db_connector.ini_connect(self.__db_name), verbose=verbose)
        self.model = self.db
        if 'charset' in self.db._cog_params:
            self._charset = self.db._cog_params['charset']
        if self.db_name != 'collorg_db':
            self.__set_app_path()
        self.__load_actions()
        self._url = "{}://{}/{}".format(
            self.db._cog_params['_url_scheme'],
            self.db._cog_params['_server_name'],
            self.db._cog_params['_script_name'])

    def new_connection(self):
        self.db.new_connection()

    def _webify(self, cog_dwi=False):
        def nothing(*args, **kwargs):
            pass
        def yes_function(a, b):
            return True
        self.user = None
        self.get_page_ref = nothing
        self.check = yes_function
        self.cog_dwi = cog_dwi
        self._site_name = "{}://{}".format(
            self.db._cog_params['_url_scheme'],
            self.db._cog_params['url'])

    def check_visibility(self, obj):
        if obj.cog_oid_.value and not obj.check_visibility(
            cog_user=self.user):
                return False
        return True

    def __set_app_path(self):
        tmp = __import__(
            'collorg_app', globals(), locals(), [], -1)
        Controller.app_path = "{}/{}".format(
            tmp.__path__[0], self.db_name)

    @property
    def app_path(self):
        return self.__app_path

    @property
    def collorg_path(self):
        return self.__collorg_path

    def clear(self):
#        open("/tmp/cog_sql", "w")
#        open("/tmp/cog_trace", "w")
        self._kwargs = {}
        self._cog_ajax = None
        self._cog_method = None
        self._cog_fqtn_ = None
        self._cog_oid_ = None
        self._cog_cmd = None
        self.__user_actions = None
        self.db.rollback()
        self.__now = datetime.datetime.now()

    @property
    def db_name(self):
        return self.__db_name

    @property
    def app_module(self):
        db_name = self.db_name
        if db_name == "collorg_db":
            return "collorg"
        return "collorg_app.{}".format(db_name)

    @property
    def repos_path(self):
        return self.__repos_path

    def process(self):
        raise NotImplementedError

    def _cog_inherits(self, obj):
        # pb with table._cog_inherits
        if obj.fqtn in self.__d_inh_tree:
            return self.__d_inh_tree[obj.fqtn]
        inh_tree = []
        for elt in type.mro(type(obj)):
            if elt is Table:
                break
            inh_tree.append(elt(obj.db))
        self.__d_inh_tree[obj.fqtn] = inh_tree
        return inh_tree

    def check_required(self, action, **kwargs):
        """
        The execution of an action can be constained by the result of
        another action (the check action).
        The constraint is considered ok if the "check action" returns any
        not empty string.
        This mecanism is used to display write icons in the header only if
        a user has write access to the data.
        """
        action_oid = action.cog_oid_.value
        ok = True
        if action_oid in self._d_check:
            required = self._d_check[action_oid]
            for elt in required:
                check_action = self._d_actions_by_oid[elt]
                res = eval("kwargs['env'].{}(**kwargs)".format(
                    check_action.name_)).strip()
                if not res:
                    ok = False
                    break
        return ok

    def __in_interval(self, interval):
        cog_from, cog_to = interval
        return ((cog_from == None or cog_from < self.__now) and
            (cog_to == None or cog_to > self.__now))

    def _unicode(self, str_):
        if type(str_) is not unicode:
            return unicode("%s" % str_, self._charset)
        return str_

    def add_json_res(self, dict_):
        """shouldn't we call this method set_json_res"""
        for key, val in dict_.items():
            self._json_res[key] = self._unicode(val)

    def __load_actions(self):
        actions = self.db.table('collorg.application.action')
        for action in actions:
            self._d_actions[(
                action.name_.value, action.data_type_.value)] = action
            self._d_actions_by_oid[action.cog_oid_.value] = action
        check = self.db.table('collorg.application.check')
        check.cog_light = True
        for elt in check:
            if not elt.requires_ in self._d_check:
                self._d_check[elt.requires_] = []
            self._d_check[elt.requires_].append(elt.required_)
