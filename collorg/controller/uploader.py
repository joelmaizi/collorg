"""
the uploader uploads (surprise) a file and attaches it to a data.
A file is only uploaded ounce
"""

import os
import sys
if sys.version >= '3':
    unicode = str
import unicodedata
import json

class Uploader(object):
    def __init__(self, controller):
        self.db = controller.db
        self.file_name = None
        self.sfile_name = None
        self.user = None
        self.data = None

    def reset(self):
        self.file_name = None
        self.sfile_name = None
        self.user = None
        self.data = None

    def __unicode(self, str_):
        if type(str_) is not unicode:
            return unicode("%s" % str_, "utf-8")
        return str_

    def strip_accents(self, str_):
        return unicodedata.normalize(
            'NFKD', str_).encode('ASCII', 'ignore').decode('ASCII')

    def attach_file(self):
        attach = self.db.table('collorg.communication.attachment')
        attach.attach(self.file_, self.data, self.user, self.description)

    def load_file(self, fs):
        __json = {}
        def fbuffer(f, chunk_size=10000):
           while True:
              chunk = f.read(chunk_size)
              if not chunk: break
              yield chunk

        fileitem = fs['file']
        session_key = fs['cog_session'].value
        self.user = self.db.table(
            'collorg.web.session', key_ = session_key)._user_.get()
        target = fs['cog_target'].value
        data_oid = fs['cog_oid_'].value
        self.data = self.db.table(
            'collorg.core.base_table', cog_oid_ = data_oid).get()
        self.description = ''
        if 'description_' in fs:
            self.description = fs['description_'].value
        self.file_name = unicode(
            os.path.basename(fileitem.filename), encoding='utf-8')
        self.sfile_name = self.strip_accents(self.file_name)
        tmp_file_name = '%s/tmp/%s' % (
            self.db._cog_params['upload_dir'], self.sfile_name)
        with open(tmp_file_name, 'wb', 10000) as tmp_file:
            for chunk in fbuffer(fileitem.file):
                tmp_file.write(chunk)
        file_ = self.db.table('collorg.communication.file')
        self.file_ = file_.upload(
            self.file_name, self.sfile_name, self.user).get()
        self.attach_file()
