#!/usr/bin/env python
#-*- coding: utf-8 -*-

### This file is part of collorg

### collorg is free software: you can redistribute it and/or modify
### it under the terms of the GNU General Public License as published by
### the Free Software Foundation, either version 3 of the License, or
### (at your option) any later version.

### This program is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
### GNU General Public License for more details.

### You should have received a copy of the GNU General Public License
### along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
from random import randint
import pydot
from collorg.controller.controller import Controller
from collorg.templates.document_type.html import Html

def short_name(node):
    return node.split('.')[-1].capitalize()

def href_ns(name):
    return "/{}/relational.svg".format(get_dir(name))

def get_dir(name):
    return "doc/graph/{}".format(name.replace(".", "/"))

index_html = """
<h1>{}</h1>
<object class="embeded" type="image/svg+xml" data="{}">
</object>
"""

class MCluster:
    def __init__(self, obj, name, R=None, G=None, B=None, parent=None):
        self.R = R or 'ff'
        self.G = G or 'ff'
        self.B = B or 'ff'
        self.cluster = pydot.Cluster(
                name.replace(".", "_"), label=name.upper(), tooltip=name,
                href=href_ns(name), style='filled')
        if not parent:
            self.set_color()
        else:
            if self.R == 'ff' and self.G == 'ff' and self.B == 'ff':
                self.set_color(parent)

    def set_color(self, parent=None):
        if parent:
            self.R = parent.R == 'ff' and 'ff' or self.__get_darker(parent.R)
            self.G = parent.G == 'ff' and 'ff' or self.__get_darker(parent.G)
            self.B = parent.B == 'ff' and 'ff' or self.__get_darker(parent.B)
        color = "#{}{}{}".format(self.R, self.G, self.B)
        self.cluster.set_color(color)
        self.cluster.set_fillcolor(color)

    def __get_darker(self, shade):
        if shade == '00':
            return '00'
        shades = ['ff', 'ee', 'dd', 'cc', 'bb', 'aa', '99', '88',
                  '77', '66', '55', '44', '33', '22', '11', '00']
        return shades[shades.index(shade) + 2]

class Cmd():
    def __init__(self, controller, *args):
        self.d_nodes_by_name = {}
        self.__ctrl = controller
        if self.__ctrl is None:
            self.__ctrl = Controller()
        self.__ctrl._webify()
        self.__draw_ns()
        namespaces = self.__ctrl.db.relation('collorg.core.namespace')
        namespaces.order_by(namespaces.name_)
        for ns in namespaces:
            self.__draw_ns(ns=ns)
            for tbl in ns.tables:
                if tbl.tablekind_.value == 'v':
                    continue
                self.__draw_ns(ns=ns, tbl=tbl)

    def __get_node(self, table_name, color='white'):
        table = self.__ctrl.db.relation('collorg.core.data_type', fqtn_=table_name).get()
        if not table_name in self.d_nodes_by_name.keys():
            self.d_nodes_by_name[table_name] = pydot.Node(
                table_name, label=short_name(table_name), tooltip=table_name,
                href=href_ns(table_name))
        node = self.d_nodes_by_name[table_name]
        node.set_color(color)
        node.set_fillcolor(color)
        node.set_style('filled')
        return self.d_nodes_by_name[table_name]

    def __draw_inh_tree(self, table_name, dgraph):
        tbl = self.__ctrl.db.table(table_name)
        on = self.__get_node(table_name)
        for parent in tbl.parents_fqtns():
            if parent == table_name:
                continue
            if parent.split('.')[-1] == 'oid_table':
                continue
            nn = self.__get_node(parent)
            dgraph.add_edge(pydot.Edge(
                on, nn, arrowhead="onormal", color="brown", arrowsize=2))
            on = nn
        on = self.__get_node(table_name)
        for child in tbl.children_fqtns():
            print(child)
            if child == table_name:
                continue
            nn = self.__get_node(child)
            dgraph.add_edge(pydot.Edge(
                nn, on, arrowhead="onormal", color="brown", arrowsize=2))

    def __draw_ns(self, ns=None, tbl=None):
        l_schema_nodes = []
        l_subclusters_nodes = []
        l_keep = []
        in_clusters = []
        self.d_nodes_by_name = {}
        d_clusters = {}
        d_subclusters = {}
        d_nodes = {}
        da_nodes = {}
        db = self.__ctrl.db
        obj = db.relation('collorg.core.database').get()
        db_name = db.name
        table_name = schema_name = ""
        if tbl is not None:
            obj = tbl
            table_name = tbl.fqtn_.value
        if ns is not None:
            obj = ns
            schema_name = ns.name_.value
        dgraph = pydot.Dot(
            table_name or schema_name or db_name, graph_type='digraph',
            label=db.name, href=href_ns(""),
            rankdir="LR", nodesetp=.75, splines="ortho")
        dgraph.set_edge_defaults(arrowhead="vee")
        dgraph.set_node_defaults(shape="box")
        if tbl is not None:
            self.__draw_inh_tree(table_name, dgraph)
        graph = db._di_graph
        name = db.name
        cluster = MCluster(obj, name, R='dd', G='ff', B='dd')
        print("graph {}".format(table_name or schema_name or db_name))
        if schema_name:
            name = schema_name
            cluster = MCluster(obj, name, R='dd', G='ff', B='dd')
            d_clusters[name] = cluster
            for node in graph.nodes():
                namespace = node.rsplit('.', 1)[0]
                if namespace == schema_name:
                    if table_name and node != table_name:
                        continue
                    l_schema_nodes.append(node)
                elif namespace.find(schema_name) == 0:
                    if not namespace in d_subclusters.keys():
                        ns_obj = self.__ctrl.db.relation(
                            'collorg.core.namespace', name_=namespace).get()
                        d_subclusters[namespace] = MCluster(
                            ns_obj, namespace, parent=d_clusters[schema_name])
                    if table_name and node != table_name:
                        continue
                    nnode = self.__get_node(node)
                    d_subclusters[namespace].cluster.add_node(nnode)
                    self.d_nodes_by_name[node] = nnode
                    l_subclusters_nodes.append(node)
            for node in graph.nodes():
                if node in l_schema_nodes or node in l_subclusters_nodes:
                    l_keep.append(node)
                    continue
                else:
                    for edge in graph.edges():
                        n1, n2 = edge
                        if ((n1 in l_schema_nodes or
                                n1 in l_subclusters_nodes) and
                                not n2 in l_keep):
                            l_keep.append(n2)
                        if ((n2 in l_schema_nodes or
                                n2 in l_subclusters_nodes) and
                                not n1 in l_keep):
                            l_keep.append(n1)
        else:
            l_keep = graph.nodes()
        for node in graph.nodes():
            if not node in l_keep:
                continue
            nnode = self.__get_node(node)
            if node.find(name) == 0:
                if table_name == node:
                    nnode.set_fontcolor('white')
                    nnode.set_color('green')
                    nnode.set_fillcolor('green')
                cluster.cluster.add_node(nnode)
            else:
                namespace = node.rsplit('.', 1)[0]
                ns_obj = self.__ctrl.db.relation(
                    'collorg.core.namespace', name_=namespace).get()
                if not namespace in d_clusters:
                    d_clusters[namespace] = MCluster(
                        ns_obj, namespace, R="dd", G="dd", B="dd")
                d_clusters[namespace].cluster.add_node(nnode)
        # clusters in clusters
        l_clusters = d_clusters.keys()
        for key, clstr in d_clusters.items():
            for subclstr in l_clusters:
                if (subclstr.find(key) == 0 and
                        len(subclstr.split('.')) == len(key.split('.')) + 1):
                    if not subclstr == schema_name:
                        d_clusters[subclstr].set_color(clstr)
                    clstr.cluster.add_subgraph(d_clusters[subclstr].cluster)
                    in_clusters.append(subclstr)
        for clstr in d_subclusters.values():
            cluster.cluster.add_subgraph(clstr.cluster)
        for key, clstr in d_clusters.items():
            if key in in_clusters:
                continue
            dgraph.add_subgraph(clstr.cluster)
        dgraph.add_subgraph(cluster.cluster)
        for node in graph.nodes():
            if not node in l_keep:
                continue
            if node.split('.')[-1] == 'oid_table':
                self.d_nodes_by_name[node].set_fontcolor('red')
        i = 0
        for edge in graph.edges(data=True):
            i += 1
            n1, n2, data = edge
            if not n1 in l_keep or not n2 in l_keep:
                continue
            for label in data['l_fields'].keys():
                label_ = "{}_".format(label)
                node1 = self.d_nodes_by_name[n1]
                node2 = self.d_nodes_by_name[n2]
                c1 = randint(0, 130)
                c = randint(130, 255)
                color = "#{:02X}{:02X}{:02X}".format(c1, c1, c)
                dgraph.add_edge(pydot.Edge(
                    node1, node2, xlabel=label_,
                    color=color, fontcolor=color))
        name = table_name or schema_name or db.name
        dir = get_dir(table_name or schema_name or '')
        if not os.path.exists(dir):
            os.makedirs(dir)
        try:
            dgraph.write_svg("{}/relational.svg".format(dir))
            with open("{}/index.html".format(dir), "w") as f_:
                f_.write(index_html.format(
                    name, "{}/relational.svg".format(href_ns(name))))
        except:
            pass
