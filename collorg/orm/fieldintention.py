#!/usr/bin/env python
#-*- coding: utf-8 -*-

### Copyright © 2011,2014 Joël Maïzi <joel.maizi@lirmm.fr>
### This file is part of collorg

### collorg is free software: you can redistribute it and/or modify
### it under the terms of the GNU General Public License as published by
### the Free Software Foundation, either version 3 of the License, or
### (at your option) any later version.

### This program is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
### GNU General Public License for more details.

### You should have received a copy of the GNU General Public License
### along with this program.  If not, see <http://www.gnu.org/licenses/>.

from .customerror import CustomError
from copy import deepcopy

class FieldIntention(object):
    __l_comp = [
        '=', '!=',
        '>', '>=', '<', '<=',
        'is', 'is not',
        'like', 'not like',
        'ilike', 'not ilike',
        '~', '~*', '!~', '!~*',
        'in', 'not in' ]
    def __init__(self, field):
        self.__field = field
        self.__val = None
        self.__raw_value = None
        self.__null = False # True if set_null or set_not_null !!
        self.__neg = False
        self.__quoted_val = ''
        self.__comp = '='
        self.__is_constrained = False
        self.__list = []

    def set_(self, val, comp = '='):
        self.__raw_value = val
        checked = False
        if val.__class__ is self.__field.__class__:
            if comp == '=':
                comp = 'IN'
            if (self.__field.is_fkey and
                self.__field.f_table.fqtn == val._cog_table.fqtn):
                    checked = True
        if type(val) in (tuple, list):
            checked = True
        try:
            assert comp.lower() in FieldIntention.__l_comp
        except:
            raise CustomError(
                "unknown comparator '%s'\n"
                "known comparators are: %s" % (
                    comp, FieldIntention.__l_comp))
        if val is None:
            self.__val = None
            self.__is_constrained = False
            return
        self.__null = False
        self.__is_constrained = True
        self.__comp = comp
        if not checked:
            val = self.__field._cog_ref_type.check(self.__field, val)
            if val is None:
                # wiered but we get here in case of rel.join(rel)
                self.__is_constrained = False
                return
        self.__val = val
        self.__quoted_val = self.quoted_val()

    def set_null(self):
        self.__val = "NULL"
        self.__comp = "IS"
        self.__null = True
        self.__is_constrained = True

    def set_not_null(self):
        self.__val = "NULL"
        self.__comp = "IS NOT"
        self.__null = True
        self.__is_constrained = True

    @property
    def is_constrained(self):
        return self.__is_constrained

    @property
    def comp(self):
        return self.__comp

    @property
    def val(self):
        return self.__val

    @property
    def value(self):
        return self.__raw_value

    def quoted_val(self, val = None):
        """
        XXX THIS METHOD SHOULD NOT ESCAPE THE VALUE!
        Instead, it should agregate all values in a list to be used as the
        second argument of cursor.execute...

        @return: the escaped SQL value.
        @raise exception: if the value can't be escaped.

        Warning! There is some recursivity here (4 levels) a nightmare
        relation._cog_SQL_select
        -> relation._cog_SQL_where (recursive)
         -> relation.SQL_where_inner
          -> field._sql_where_repr
           -> fieldintention._sql_where_repr (recursive)
            -> fieldintention.__single_sql_where_repr
             -> fieldintention.quoted_val (recursive)
              -> relation._cog_SQL_select (recursive (see top))

        table._cog_SQL_insert -> field.quoted_val  -> this method

        table.__sql_update -> field.quoted_val -> this method

        The values should not be escaped.
        """
        if not self.__is_constrained:
            return None
        val = val or self.__val
        if not self.__null:
            if val.__class__ is self.__field.__class__:
                table = val.table
                ref_field = val
                if (self.__field.is_fkey and
                    self.__field.f_table.fqtn == val._cog_table.fqtn):
                    ref_field = table.__dict__[self.__field.f_fieldname]
                return table._cog_SQL_select([ref_field])
            elif type(val) != bool and val is not None:
                if type(val) not in (tuple, list):
                    val = str(val).replace("'", "''").replace('%', '%%')
                        #.replace("\\", "\\\\")
                    return "'%s'" % (val)
                else:
                    if len(val) > 1:
                        self.__comp = 'in'
                        l_vals = [self.quoted_val(elt) for elt in val]
                        return [{',':l_vals}]
                    elif len(val) == 1:
                        return self.quoted_val(val[0])
            elif type(val) is bool:
                if val:
                    return "'t'"
                return "'f'"
        else:
            return val
        raise RuntimeError("Not here! %s\n" % (val))

    def __single_sql_where_repr(self):
        val = self.quoted_val()
        if val is None:
            return []
        field_name = self.__field.id_name
        comp = self.comp
        if self.__field.unaccent:
            field_name = "unaccent(%s)" % field_name
            val = "unaccent(%s::text)" % val
        return [field_name, comp, val]

    def _sql_where_repr(self, res = ""):
        if res == "":
            res = self.__single_sql_where_repr()
        for intention, op_ in self.__list:
            res.append(op_)
            res.append(intention.__single_sql_where_repr())
        if self.__neg:
            res.insert(0, "not")
        return res

    def __call__(self):
        return deepcopy(self)

    def __repr__(self):
        #!! conversion systématique en str ?
        return str(self.__val)

    def __op(self, other, _op_):
        self.__list.append((other, _op_))
        return self

    def __iop(self, _op_, val, comp):
        if not self.is_constrained:
            self.set_(val, comp)
        else:
            other = self.__class__(self.__field)
            other.set_(val, comp)
            self.__op(other, _op_)
        return self

    def __add__(self, other):
        self.__op(other, "or")
        return self

    def __iadd__(self, val, comp = 'IN'):
        if type(val) is tuple:
            val, comp = val
        self.__iop("or", val, comp)
        return self

    def __mul__(self, other):
        self.__op(other, "and")
        return self

    def __imul__(self, val, comp = 'IN'):
        if type(val) is tuple:
            val, comp = val
        self.__iop("and", val, comp)
        return self

    def __sub__(self, other):
        self.__op(other, "and not")
        return self

    def __isub__(self, val, comp = 'IN'):
        if type(val) is tuple:
            val, comp = val
        self.__iop("and not", val, comp)
        return self

    def __neg__(self):
        self.__neg = not self.__neg
