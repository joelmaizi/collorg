#!/usr/bin/env python
#-*- coding: utf-8 -*-

### Copyright © 2011,2014 Joël Maïzi <joel.maizi@lirmm.fr>
### This file is part of collorg

### collorg is free software: you can redistribute it and/or modify
### it under the terms of the GNU General Public License as published by
### the Free Software Foundation, either version 3 of the License, or
### (at your option) any later version.

### This program is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
### GNU General Public License for more details.

### You should have received a copy of the GNU General Public License
### along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
La connection se contente de charger le squelette de la base
db.schema.table sans instancier "complètement" les objets correspondants
aux tables. C'est au moment de l'instanciation que les champs seront
"accrochés". -> gain de temps.
Le graphe de la base est construit à la connexion.
"""

import sys
from datetime import datetime
import networkx as nx
import traceback
import os
from importlib import import_module

#from collorg.utils.deco import benchmark, logging, counter, trace
from .metadata import Metadata
from .schema import Schema
import psycopg2.extras

class Db(object):
    """
    blah
    """
    __d_tables = {}
    __debug = True
    def __init__(
        self, controller, db_name, conn, params, verbose=False):
        """
        """
        self.__name = db_name
        self.__conn = conn
        self.__conn.autocommit = True
        self._cog_params = params
        if self._cog_params['debug']:
            self.__debug = True
        self.__test_mode = False
        self.__transaction = []
        self._d_r_neigh = {}
        self._cog_controller = controller
        if verbose:
            sys.stderr.write("[collorg][{}] {}\n".format(
                self.__name, datetime.now().isoformat()))
        self.__in_transaction = False
        self._metadata = Metadata(self)
        self._di_graph = self.__set_graph()
        self._graph = nx.Graph(self._di_graph)
        self.__l_schemas_names = []
        for schema in self._metadata.schemas_names():
            self.__l_schemas_names.append(schema)
            self.__add_schema(schema)

    def new_connection(self):
        try:
            self.__conn.close()
        finally:
            self.__conn = psycopg2.connect(
                database=self.__name,
                user=self._cog_params['user'],
                password=self._cog_params['password'],
                host=self._cog_params['host'],
                port=self._cog_params['port']
            )

    def close(self):
        self.__conn.close()

    @property
    def conn(self):
        return self.__conn

    def __get_test_mode(self):
        return self.__test_mode

    def __set_test_mode(self, mode=True):
        assert mode in (True, False)
        if mode is True:
            print("Going into test mode.")
        if mode is False:
            bt = self.table('collorg.core.base_table')
            bt.cog_test_.value = True
            bt.cog_signature_.value = id(self)
            print(
                "Out of test mode. Removing {} test tuples.".format(bt.count()))
            bt.delete()
        self.__test_mode = mode

    test_mode = property(__get_test_mode, __set_test_mode)

    def __set_graph(self):
        """
        constructs the directed graph representing the database
        """
        heavy_nodes = [ 'collorg.core.data_type' ]
        di_graph = nx.DiGraph()
        d_fkey = self._metadata.d_fkey
        for fqtn in d_fkey:
#            print("XXXXX", fqtn,d_fkey[fqtn])
            for fk_fqtn, l_fields in d_fkey[fqtn].items():
                src = fqtn
                dst = fk_fqtn
                weight = 1
                if src in heavy_nodes or dst in heavy_nodes:
                    weight = 100
                di_graph.add_edge(src, dst)
                di_graph[src][dst]['l_fields'] = l_fields
                if not dst in self._d_r_neigh:
                    self._d_r_neigh[dst] = {}
                if not src in self._d_r_neigh[dst]:
                    self._d_r_neigh[dst][src] = []
                self._d_r_neigh[dst][src] = l_fields
        return (di_graph)

    def neighbors(self, fqtn):
        """returns only directs neighbors"""
        try:
            return self._di_graph[fqtn]
        except:
            return {}

    def rev_neighbors(self, fqtn):
        try:
            return self._d_r_neigh[fqtn]
        except:
            return {}

    def _neigh(self, fqtn):
        try:
            d_neigh = self.d_neigh[fqtn]
        except:
            d_neigh = {}
        try:
            d_r_neigh = self.d_r_neigh[fqtn]
        except:
            d_r_neigh = {}
        return d_neigh, d_r_neigh

    def __add_schema(self, schemaname):
        attr_schema = "{}_".format(schemaname)
        if not self.has_schema(attr_schema):
            self.__setattr__(attr_schema, Schema(self, schemaname))

    def has_schema(self, attr_schema):
        """
        retourne vrai si le schéma est déjà rattaché au self
        """
        return attr_schema in self.__dict__

    @property
    def metadata(self):
        return self._metadata

    def reload(self):
        raise NotImplementedError

    # SQL

    def transaction_begin(self):
        """
        All resquests (except select) will be appended to __transaction
        and executed as a single transaction until an explicit call
        to the commit or rollback method is made.
        """
        self.__in_transaction = True

    def __log_sql(self, sql, duration, err=""):
        if self._cog_controller._debug:
            trace = traceback.extract_stack()[:-3]
            f = open("/tmp/cog_sql", 'a+')
            try:
                os.chmod("/tmp/cog_sql", 0o777)
            except:
                pass
            f.write("{}\n{}\n{}\n{}\n".format(
                "{}{} {}s".format(err, 60 * "=", duration.total_seconds()),
                "\n".join(["{} {} {} {}".format(*elt) for elt in trace]),
                70*"-", sql))
            f.close()

    def commit(self, sql="", just_return_sql=False):
        """
        All requests in __transaction are executed in a single transaction.
        A rollback is issued on error.
        """
        sql = sql or ";\n".join([elt[0] for elt in self.__transaction])
        if not sql or just_return_sql:
            return sql
        begin = datetime.now()
        try:
            cursor = self.__conn.cursor(
                cursor_factory = psycopg2.extras.DictCursor)
            if sql:
                cursor.execute(sql)
            else:
                for req, vals in self.__transaction:
                    cursor.execute(req, vals)
            self.__conn.commit()
            duration = datetime.now() - begin
        except Exception as err:
            duration = datetime.now() - begin
            self.rollback()
            self.__log_sql(sql, duration, "ERROR")
            raise Exception("Commit error! Rolling back!\n{}\n{}".format(
                err, sql))
        finally:
            cursor.close()
        self.__log_sql(sql, duration)
        self.__transaction = []
        self.__in_transaction = False

    def fetchone(self, sql):
        cursor = self.__conn.cursor(
            cursor_factory = psycopg2.extras.DictCursor)
        try:
            cursor.execute(sql)
            return cursor.fetchone()
        finally:
            cursor.close()

    def rollback(self):
        self.__in_transaction = False
        self.__transaction = []
        self.__conn.rollback()

    def raw_sql(self, sql_req, vals, nodelay=False):
        """
        Appends the request sql_req in __transaction. if __in_transaction
        is False, a commit is immediately issued.
        """
        self.__transaction.append((sql_req, vals))
        if not self.__in_transaction:
            self.commit(nodelay and sql_req or "")

    def get_query_res(self, sql_req, vals):
        cursor = self.__conn.cursor(
            cursor_factory = psycopg2.extras.DictCursor)
        try:
            cursor.execute(sql_req, vals)
            return cursor.fetchall()
        finally:
            cursor.close()

    @property
    def name(self):
        return self.__name

    @property
    def schemas_names(self):
        return self.__l_schemas_names

    def _fqtn_2_sql_fqtn(self, fqtn):
        return '"{}"'.format('"."'.join(fqtn.rsplit('.', 1)))

#    @counter
#    @trace
    def table(self, cog_fqtn, *args, **kwargs):
        """
        le fqtn est une chaîne de la forme : {db:}<schema>.<table>
        si db est spécifié, il s'agit d'une base distante [NI]
        si le nom du schéma débute par collorg, il s'agit du schema collorg
        exemple : table('a.b')
        l'objet sera référencé par self.a_.b_ (PEP 8)
        """
        # sys.stderr.write("DEBUG db.table {}\n".format(cog_fqtn))
        import collorg
        if not cog_fqtn in self.__d_tables:
            cog_fqtn = cog_fqtn.replace('"', '')
            (schema_name, table_name) = cog_fqtn.rsplit(".", 1)
            if schema_name.find('collorg') == 0:
                base_mod = 'collorg'
                mod_path_1 = 'db'
                schema_name = ".".join(schema_name.split('.')[1:])
            else:
                import collorg_app
                base_mod = 'collorg_app'
                mod_path_1 = "{}.db".format(self.__name)
            class_name = "{}".format(table_name.capitalize())
            module_name = ".".join([mod_path_1, schema_name, table_name])
            temp = import_module(".{}".format(module_name), base_mod)
            self.__d_tables[cog_fqtn] = temp.__dict__[class_name]
        table = self.__d_tables[cog_fqtn](self, *args, **kwargs)
        return table

    relation = table

    def _set_controller(self, controller):
        self._cog_controller = controller

    @property
    def schemas(self):
        for elt in self.__dict__:
            if self.__dict__[elt].__class__ is Schema:
                yield self.__dict__[elt]

    @property
    def tables(self):
        for sch in self.schemas:
            for table in sch.tables:
                yield table

    @property
    def fqtns(self):
        for sch in self.schemas:
            for fqtn in sch.fqtns:
                yield fqtn

    def get_elt_by_oid(self, cog_oid):
        """
        """
        return self.table(
            'collorg.core.base_table', cog_oid_=cog_oid).get()

    def _sql_inherits(self, fqtn):
        """returns the list of fqtn inherited by self in db metadata"""
        res = []
        oid = self.metadata.d_fqtn_table[fqtn]
        for elt in self.metadata.d_oid_table[oid]['inherits']:
            res.append(self.metadata.d_oid_table[elt]['fqtn'])
        return res

    def showstruct(self, schema_name=None):
        schemas = list(self.schemas)
        schemas.sort()
        for schema in schemas:
            if schema_name is not None and schema_name != schema.name:
                continue
            print("\nSchema: {}\n".format(schema.name))
            tables = list(schema.tables)
            tables.sort()
            for table in tables:
                print(" - {}".format(table))

    def what_references(self, ref, fqtn=None):
        dtypes = self.relation('collorg.core.data_type')
        dtypes.cog_fqtn_.value = fqtn
        for dtype in dtypes:
            rel = self.relation(dtype.fqtn_.value)
            fk = False
            idx = 0
            for field in rel._cog_foreign_keys:
                if idx:
                    srel = self.relation(dtype.fqtn_.value)
                    try:
                        srel.__dict__[field.pyname].value = ref
                        fk = True
                    except:
                        continue
                    rel += srel
                else:
                    try:
                        rel.__dict__[field.pyname].value = ref
                        fk = True
                    except:
                        continue
                idx += 1
            if fk:
                count = rel.count()
                if count:
                    yield(rel, count)

    def search(self, value, fqtn=None):
        """slow. search the value in the whole database"""
        dtypes = self.relation('collorg.core.data_type')
        dtypes.fqtn_.value = fqtn
        for dtype in dtypes:
            rel = self.relation(dtype.fqtn_.value)
            fk = False
            idx = 0
            for field in rel._cog_fields:
                try:
                    if field.ref_type._type == bool:
                        continue
                    elif field.ref_type._type == str:
                        ivalue  = '%{}%'.format(value)
                except:
                    #XXX BUG with enum fields
                    continue
                if idx:
                    srel = self.relation(dtype.fqtn_.value)
                    try:
                        srel.__dict__[field.pyname].value = (ivalue, 'ilike')
                        rel += srel
                        fk = True
                    except:
                        continue
                else:
                    try:
                        rel.__dict__[field.pyname].value = (ivalue, 'ilike')
                        fk = True
                    except:
                        continue
                idx += 1
            if fk:
                try:
                    count = rel.count()
                except:
                    continue
                if count:
                    yield(rel, count)
