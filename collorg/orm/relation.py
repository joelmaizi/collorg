#!/usr/bin/env python
#-*- coding: utf-8 -*-

### Copyright © 2011,2014 Joël Maïzi <joel.maizi@lirmm.fr>
### This file is part of collorg

### collorg is free software: you can redistribute it and/or modify
### it under the terms of the GNU General Public License as published by
### the Free Software Foundation, either version 3 of the License, or
### (at your option) any later version.

### This program is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
### GNU General Public License for more details.

### You should have received a copy of the GNU General Public License
### along with this program.  If not, see <http://www.gnu.org/licenses/>.

import inspect
import gettext
from collections import OrderedDict
import json

from .field import Field
from .customerror import CustomError
import sys

def trace(debug, arg):
    if debug:
        sys.stderr.write("%s\n" % (arg))

class LObj():
    def __init__(self, d_elt, ctrl, safe=True):
        self._cog_controller = ctrl
        self.fqtn = None
        for key, val in d_elt.items():
            if type(val) is str and safe:
                val = val.replace('>', '&gt;').replace('<', '&lt;')
            if key == 'cog_fqtn_':
                self.fqtn = val
            self.__dict__[key] = val

    def raw(self):
        """returns the dict. Used by cog dump"""
        return self.__dict__

class Relation(object):
    """
    Public methods:
    * select
     * cog_limit
     * cog_offset
     * dict_order_by
    * get
    * count
    * min
    * max
    * dict_by
    """
    def __init__(self, _db):
        if _db:
            self.db = _db
        self.__sql_req = ""
        self._cog_controller = self.db._cog_controller
        self._ = self.db._cog_controller.i18n.gettext
        self.cog_distinct = False
        self.__id = "r_%s" % id(self)
        self.__extension = list()
        self.__retrieved = False
        self.__light = False
        self._list = list()
        self._cog_limit = None
        self._cog_offset = None
        self.__negation = False
        self.relation = self.db.table

    @property
    def fqtn(self):
        return '%s.%s' % (self._cog_schemaname, self._cog_tablename)

    @property
    def sql_fqtn(self):
        return '"%s"."%s"' % (self._cog_schemaname, self._cog_tablename)

    @property
    def cog_type_name(self):
        cn = self.__class__.__name__
        tna = '_{}__cog_type_name'.format(cn)
        if hasattr(self, tna):
            return getattr(self, tna)
        return self.fqtn

    @property
    def sql_req(self):
        return self.__sql_req

    @property
    def id(self):
        return self.__id

    def __cog_light(self, value):
        """
        sets self.__light result to value (True of False)
        if sets to True, the select tuples are returned as dict. Otherwise
        they are instanciated (slow)
        """
        assert value in (True, False)
        self.__light = value

    cog_light = property(fset=__cog_light)

    def dict_by(self, *fields):
        # self represents the extension
        from collections import OrderedDict
        _dict = OrderedDict()
        field_names = [ field.name for field in fields ]
        for elt in self:
            if not self.__light:
                key = tuple("%s" % elt.__dict__["%s_" % (field_name)].value
                    for field_name in field_names)
                if not key in _dict:
                    _dict[key] = []
                _dict[key].append(elt)
            else:
                key = tuple("%s" % elt.__dict__["%s_" % (field_name)]
                    for field_name in field_names)
                if not key in _dict:
                    _dict[key] = []
                _dict[key].append(elt)
        return _dict

    def order_by(self, *args):
        self._cog_order_by = []
        for elt in args:
            if elt is None:
                continue
            try:
                assert elt.__class__ is Field
                self._cog_order_by.append(elt)
            except Exception as e:
                raise RuntimeError("%s\nwrong elt %s. expected Field\n" % (
                    e, elt.__class__.__name__))
        return self

    def cog_limit(self, limit):
        if limit:
            assert type(limit) == int and limit >= 0
            self._cog_limit = limit

    def cog_offset(self, offset):
        if offset:
            assert type(offset) == int and offset >= 0
            self._cog_offset = offset

    @property
    def _cog_extension(self):
        return self.__extension

    def reset(self):
        """
        clear the extension and intention of the self object.
        All the fields are cleared.
        The extention is set to empty list
        """
        for field in self._cog_fields:
            self.__dict__[field.pyname] = field()
        self.__extension = list()

    def _cog_SQL_select(self, fields=None, count=False):
        """
        TEST CONSTRUCTION REQUETE SELECT. SANS DOUTE PAS ICI!!
        """
        if fields is None:
            fields = self._cog_fields
        else:
            self.cog_distinct = True
        req = ["SELECT\n"]
        l_fields_names = []
        l_fields = []
        for field in fields:
            l_fields_names.append(field.name)
            if not count:
                l_fields.append("%s.%s" % (field.table.id, field.sql_name_as))
            else:
                l_fields.append("%s.%s" % (field.table.id, field.orig_name))
        assert len(l_fields) > 0
        distinct = ""
        if self.cog_distinct:
            distinct = "DISTINCT\n"
        what_req = {",":l_fields}
        if count:
            req.append(["COUNT\n",["DISTINCT\n",[what_req]]])
        elif self.cog_distinct:
            req.append("DISTINCT\n")
            req.append(what_req)
        else:
            req.append(what_req)
        req.append("FROM\n")
        req.append("{}\n".format(self._cog_SQL_from()))
        if self._cog_is_constrained():
            req += self._cog_SQL_where()
        o_req = []
        missing_field = False
        if not count and self._cog_order_by:
            o_req.append("ORDER BY\n")
            o_req.append(",".join([
                #"%s.%s"%(field.table.id, field.name)
                '"%s" %s' % (field.name, field.descending_order)
                for field in self._cog_order_by
            ]))
            if not field.name in l_fields_names:
                missing_field = True
        if not missing_field:
            req.append("\n".join(o_req))
        if self._cog_limit is not None:
            req.append(" LIMIT {}\n".format(self._cog_limit))
        if self._cog_offset is not None:
            req.append(" OFFSET {}\n".format(self._cog_offset))
        return req
        return "\n".join(req)

    def _cog_SQL_count(self, fields=None):
        return self._cog_SQL_select(fields=fields,count=True)

    def _cog_SQL_where(self, id_=None):
        def SQL_where_inner(self, rel_id=None):
            """
            @return: SQL form of where condition
            """
            def _just_cog_oid(self):
                for field in self._cog_fields:
                    if field.name == 'cog_oid' and field.value != None:
                        return field
            l_where = []
            cog_oid = _just_cog_oid(self)
            if cog_oid:
                l_where.append(cog_oid._sql_where_repr(rel_id))
            else:
                for field in self._cog_fields:
                    sql_where_repr = field._sql_where_repr(rel_id)
                    if sql_where_repr:
                        l_where.append(sql_where_repr)
            return  {'AND\n': l_where}

        req = []
        if not self._list:
            if self.__negation:
                req.append("NOT")
            req.append(SQL_where_inner(self, id_ or self.id))
        else:
            sub_req = []
            elt1 = self._list[1]
            if elt1.__negation:
                sub_req.append("NOT")
            if elt1._list:
                sub_req.append(elt1._cog_SQL_where(id_ or self.id))
            else:
                sub_req.append(SQL_where_inner(elt1, id_ or self.id))

            sub_req.append(self._list[0])

            elt2 = self._list[2]
            elt2.__negation and sub_req.append("NOT")
            if elt2._list:
                sub_req.append(elt2._cog_SQL_where(id_ or self.id))
            else:
                sub_req.append(SQL_where_inner(elt2, id_ or self.id))
            req.append(sub_req)
        if not id_:
            if self.__negation:
                req.insert(0, "NOT")
            req.insert(0, "WHERE\n")
        return req

    def _cog_is_constrained(self):
        """
        @return: True if at least one of the fields of self is constrain,
        False otherwise.
        """
        if self._list:
            return True
        for field in self._cog_fields:
            if field.is_constrained:
                return True
        return False

    def _cog_SQL_gen_request(self, sql_req):
        def SQL_gen_request_list(self, sql_req, depth=0):
            sql_req_list = []
            #sql_req_list.append("-- XXX {}\n".format(sql_req))
            if type(sql_req) is list:
                for elt in sql_req:
                    if type(elt) is list or type(elt) is tuple:
                        sql_req_list.append("\n{}(\n".format(" "*(depth + 2)))
                        sql_req_list += SQL_gen_request_list(
                            self, elt, depth=depth + 4)
                        sql_req_list.append("{})\n".format(" "*(depth + 2)))
                    elif type(elt) is dict:
                        last = ""
                        for key, vals in elt.items():
                            sub_sql_req_list = []
                            for val in vals:
                                if type(val) is list or type(val) is tuple:
                                    sub_sql_req_list.append("  (")
                                    sub_sql_req_list += SQL_gen_request_list(
                                        self, val, depth=2)
                                    sub_sql_req_list.append("  )")
                                elif type(val) is dict:
                                    raise NotImplementedError
                                else:
                                    sub_sql_req_list.append(val)
                                sub_sql_req_list.append(key)
                            sub_sql_req_list.pop()
                            for sub_elt in sub_sql_req_list:
                                sql_req_list.append("{}{}\n".format(
                                    " "*(depth + 2), sub_elt.strip('\n')))
                    else:
                        if elt.strip():
                            sql_req_list.append("{}{}".format(" "*depth, elt))
            elif type(sql_req) is tuple:
                sql_req_list.append("{} {} ".format(
                    sql_req[0], sql_req[1].strip('\n')))
                if len(sql_req) > 2:
                    if type(sql_req[2]) is not str:
                        sql_req_list.append("(")
                        sql_req_list += SQL_gen_request_list(
                            self, sql_req[2], depth=2)
                        sql_req_list.append(")")
                    else:
                        sql_req_list.append("  {}".format(sql_req[2]))
            else:
                return sql_req
            return sql_req_list

        sql = "".join(SQL_gen_request_list(self, sql_req))
        #XXX The NOTNOT bug!
        lines = []
        onot = False
        for line in sql.split("\n"):
            lines.append(line.replace("NOTNOT", "NOT"))
        return "\n".join(lines)

    def select(
        self, expected=-1, fields=None, just_return_sql=False):
        sql_req = "".join(
            self._cog_SQL_gen_request(self._cog_SQL_select(fields=fields)))
        if just_return_sql:
            return sql_req
        try:
            extension = self.db.get_query_res(sql_req, ())
        except Exception as e:
            self.db.rollback()
            raise CustomError("select error: %s\n%s" % (e, sql_req))
        if expected != -1:
            try:
                assert len(extension) == expected
            except:
                raise CustomError("expected %s, got %s tuples\n%s" % (
                        expected, len(extension), sql_req))
        self.__extension = extension
        self.__retrieved = True
        return self

    def get(
        self, fields=None, just_return_sql=False, recurse=True,
        reload_=False):
        if reload_:
            assert self.cog_oid_.value is not None
            for field in self._cog_fields:
                if field.name != 'cog_oid':
                    self.__dict__[field.pyname].value = None
        #!! offset ?
        res = self.select(
            expected=1, fields=fields, just_return_sql=just_return_sql)
        if just_return_sql:
            return res
        for key, val in self.__extension[0].items():
            self.__dict__["%s_" % (key)].value = val
        self.__retrieved = True
        self.__uniq = True
        if 'cog_fqtn_' in self.__dict__ and self.fqtn != self.cog_fqtn_.value:
            obj = self.db.table(self.cog_fqtn_.value)
            obj.cog_oid_.value = self.cog_oid_.value
            return obj.get()
        return self[0]

    def _cog_count(self, orig, fields, just_return_sql):
        """
        retourne le nombre d'éléments dans la base de donnée correspondant
        à l'intention posée
        """
        #
        sql_req = self._cog_SQL_gen_request(self._cog_SQL_count(fields=fields))
        if just_return_sql:
            return sql_req
        try:
            return self.db.get_query_res(sql_req, ())[0][0]
        except Exception as e:
            self.db.rollback()
            raise CustomError("Count error: %s\n%s" % (e, sql_req))

    def count(self, fields=None, just_return_sql=False):
        return self._cog_count(
            orig=self, fields=fields, just_return_sql=just_return_sql)

    def is_empty(self):
        sql = self.select(just_return_sql=True)
        sql += "\nLIMIT 1"
        exists = self.db.fetchone(sql) and True or False
        return not exists

    def exists(self):
        sql = self.select(just_return_sql=True)
        sql += "\nLIMIT 1"
        return self.db.fetchone(sql) and True or False

    def __min_max(self, min_max, field):
        req = []
        req.append('select {}(array["{}"])'.format(min_max, field.name))
        req.append('from {}'.format(self._cog_SQL_from()))
        req.append(self._cog_SQL_gen_request(self._cog_SQL_where()))
        sql_req = "\n".join(req)
        res = self.db.get_query_res(sql_req, ())[0][0][0]
        return res

    def min(self, field):
        return self.__min_max('min', field)

    def max(self, field):
        return self.__min_max('max', field)

    # def __len__(self):
    #     if len(self.__extension) == 0:
    #         self.select()
    #     return len(self.__extension)

    def __iter__(self):
        if len(self.__extension) == 0:
            self.select()
        for elt in self.__extension:
            d_elt = {}
            for key, val in elt.items():
                d_elt["%s_" % (key)] = val
            if self.__light:
                yield(LObj(d_elt, self._cog_controller))
            else:
                res = self.__class__(self.db, **d_elt)
                res.__retrieved = True
                res.__uniq = True
                yield(res)

    def __getitem__(self, idx):
        d_elt = {}
        for key, val in self.__extension[idx].items():
            d_elt["%s_" % (key)] = val
        if self.__light:
            return LObj(d_elt, self._cog_controller)
        return self.__class__(self.db, **d_elt)

    def _cog_SQL_from(self, debug = False):
        return "%s %s" % (self.sql_fqtn, self.id)

    @property
    def cog_label_fields(self):
        if '_cog_label' in self.__class__.__dict__:
            return self._cog_label[1:]
        else:
            l_fields = []
            for field in self._cog_fields:
                if( field.name != 'cog_oid' and field.pkey and
                    field.sql_type != 'c_oid'):
                    l_fields.append(field)
            return l_fields

    def cog_label(self):
        fields = []
        for field in self.cog_label_fields:
            if type(field) is Field:
                if field.value is not None:
                    fields.append(field.value)
                else:
                    fields.append('')
            elif field is not None:
                fields.append(field)
            else:
                fields.append('')
        if '_cog_label' in self.__class__.__dict__:
            label = self._cog_label[0].format(*fields)
        else:
            label = ' '.join(fields)
        return label.strip()

    def __duplicate_intention(self):
        new_ = self()
        new_._list = self._list
        new_.__negation = self.__negation
        for field in self._cog_fields:
            if field.is_constrained:
                new_.__dict__[field.pyname].value = field
        return new_

    def __add__(self, other):
        if self._cog_is_constrained() and other._cog_is_constrained():
            new_ = self()
            dup_self = self.__duplicate_intention()
            dup_other = other.__duplicate_intention()
            new_._list = ("OR", dup_self, dup_other)
            return new_
        elif not self._cog_is_constrained():
            return other
        return self

    __iadd__ = __add__

    def __mul__(self, other):
        if self._cog_is_constrained() and other._cog_is_constrained():
            new_ = self()
            dup_self = self.__duplicate_intention()
            dup_other = other.__duplicate_intention()
            new_._list = ("AND", dup_self, dup_other)
            return new_
        elif not self._cog_is_constrained():
            return other
        return self

    __imul__ = __mul__

    def __sub__(self, other):
        if self._cog_is_constrained() and other._cog_is_constrained():
            new_ = self()
            dup_self = self.__duplicate_intention()
            dup_other = -(other.__duplicate_intention())
            new_._list = ("AND", dup_self, dup_other)
            return new_
        elif not self._cog_is_constrained():
            return other
        return self

    __isub__ = __sub__

    def __neg__(self):
        new_ = self.__duplicate_intention()
        new_.__negation = not self.__negation
        return new_

    def __eq__(self, other):
        return self in other and other in self

    def __ne__(self, other):
        return not(self == other)

    def __lt__(self, other):
        return self in other and not (other - self).is_empty()

    def __gt__(self, other):
        return other in self and not (self - other).is_empty()

    def __le__(self, other):
        return self in other

    def __ge__(self, other):
        return other in self

    def __contains__(self, other):
        return (other - self).is_empty()

    @staticmethod
    def __inherited_fqtns(cls):
        """
        returns the classes inherited by self up to Table
        """
        if not hasattr(cls, '__inherited_classes'):
            cls.__inherited_classes = []
            for base in cls.__bases__:
                if '_cog_schemaname' in base.__dict__:
                    fqtn = "{}.{}".format(
                        base._cog_schemaname, base._cog_tablename)
                    if not fqtn in cls.__inherited_classes:
                        cls.__inherited_classes.append(fqtn)
                    for cfqtn in cls.__inherited_fqtns(base):
                        if not cfqtn in cls.__inherited_classes:
                            cls.__inherited_classes.append(cfqtn)
            cls.__inherited_classes.insert(
                0, "{}.{}".format(cls._cog_schemaname, cls._cog_tablename))
        return cls.__inherited_classes

    def parents_fqtns(self):
        return self.__inherited_fqtns(self.__class__)

    def children_fqtns(self):
        return self.db.metadata.children_fqtns(
            self._cog_schemaname, self._cog_tablename)

    def cog_restrict_to_type(self, fqtn):
        obj = self.db.table(fqtn)
        self.cog_fqtn_ += (fqtn, '=')
        for fqtn in obj.children_fqtns():
            self.cog_fqtn_ += (fqtn, '=')

    def cog_path(self, data_type):
        return None

    @property
    def _cog_foreign_keys(self):
        """
        @return: An iterator over the fields of the self
        """
        for field in self._cog_fields:
            if field.f_table:
                yield field

    def __cog_json_handler(self, obj):
        if hasattr(obj, 'isoformat'):
            return obj.isoformat()
        else:
            raise TypeError('''Object of type {} with value of {}'''
                ''' is not JSON serializable''').format(type(obj), repr(obj))

    def _cog_json_dumps(self):
        """returns a json.dumps of self.__extension"""
        l_out = []
        for elt in self:
            out = OrderedDict()
            for field in elt._cog_fields:
                out[field.name] = field.value
            l_out.append(out)
        return json.dumps(l_out, default=self.__cog_json_handler)

    def _cog_json_loads(self, data):
        """loads data into a Relation object"""
        new = self()
        in_ = json.loads(data)
        for elt in in_:
            assert elt['cog_fqtn'] == self.fqtn
        new.__extension = in_
        return new
