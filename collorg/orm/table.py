#!/usr/bin/env python
#-*- coding: utf-8 -*-

### Copyright © 2011,2014 Joël Maïzi <joel.maizi@lirmm.fr>
### This file is part of collorg

### collorg is free software: you can redistribute it and/or modify
### it under the terms of the GNU General Public License as published by
### the Free Software Foundation, either version 3 of the License, or
### (at your option) any later version.

### This program is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
### GNU General Public License for more details.

### You should have received a copy of the GNU General Public License
### along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
import inspect
import os
import time
import uuid
from datetime import datetime
from importlib import import_module

#from collorg.utils.deco import benchmark, logging, counter
from .relation import Relation
from .field import Field
from .customerror import CustomError

class Table(Relation):
    """
    Public methods:
    * insert
    * update
    * delete
    * increment
    """
    _cog_check_method_name_re = re.compile('[^a-zA-Z0-9_]')
    _cog_base_table = False
    def __load_fields(self):
        l_fields = self.db.metadata.fields_names(
            self._cog_schemaname, self._cog_tablename)
        for fieldname in l_fields:
            attr_fieldname = "%s_" % (fieldname)
            if not fieldname in self.__dict__:
                attr_fieldname = "%s_" % (fieldname)
                setattr(self,
                         attr_fieldname,
                         Field(self, fieldname))
                self.__l_fields.append(attr_fieldname)
                if self.__dict__[attr_fieldname].pkey:
                    self.__pkey_fields.append(
                        self.__dict__[attr_fieldname].pkey)
        self.__fields_loaded = True

    def eval(self, string, *args, **kwargs):
        for arg in args:
            assert re.search(self._cog_check_method_name_re, arg) is None
        return eval(string.format(*args, **kwargs))

    def __import_templates(self):
        """
        Imports the templates (see <fqtn>/templates) as methods of
        the class.
        Collorg templates can be overloaded by the application.
        """
        def load_templates_module(module_name):
            templates = import_module(".templates", module_name)
            if not '__all__' in templates.__dict__:
                templates.__dict__['__all__'] = []
            return templates
        l_templates = []
        module_name = self.__class__.__module__
        for super_class in self.db._sql_inherits(self.fqtn):
            self.db.table(super_class)
        l_templates.append((module_name, load_templates_module(module_name)))
        if module_name.find('collorg.db') == 0:
            app_module_name = module_name.replace(
                'collorg.db', 'collorg_app.{}.db.collorg'.format(self.db.name))
            try:
                l_templates.append(
                    (app_module_name, load_templates_module(app_module_name)))
            except ImportError:
                pass
        for module_name, templates in l_templates:
            for template_name in templates.__all__:
                try:
                    temp = import_module(
                        ".{}".format(template_name),
                        "{}.templates".format(module_name)
                    )
                    setattr(
                        self.__class__, template_name,
                        self.eval("kwargs['temp'].{}", template_name, temp=temp)
                    )
                except:
                    print("WARNING! Could not import template {}.{}".format(
                        module_name, template_name
                        ))
        self.__class__._templates_imported = True

    def __init__(self, _db, trace=False, *args, **kwargs):
        Relation.__init__(self, _db)
        if not '_templates_imported' in self.__class__.__dict__:
            self.__import_templates()
        self.__neg_intention = False
        self.__extension = []
        self.__l_fields = []
        self._cog_order_by = []
        self.__retrieved = False
        self.__uniq = False
        self.__pkey_fields = []
        self.__label_fields = []
        self.__neighbors = self.db.neighbors(self.fqtn)
        self.__fields_loaded = True
        self.__load_fields()
        if kwargs:
            for fieldname, val in kwargs.items():
                self.__dict__[fieldname].value = val
        for field in args:
            self.__dict__[field.name].value = field.value, field.comp

    @property
    def _cog_is_view(self):
        return self._cog_metadata['tablekind'] == 'v'

    @property
    def _cog_metadata(self):
        oid = self.db.metadata.d_fqtn_table[self.fqtn]
        return self.db.metadata.d_oid_table[oid]

    @property
    def _cog_pkey_fields(self):
        for field in self.__pkey_fields:
            yield field

    @property
    def _cog_label_fields(self):
        for field in self.__label_fields:
            yield field

    @property
    def _cog_inherits(self):
        """returns the list of objects inherited by self"""
        return self.__class__.__bases__

    @property
    def _cog_fqtn_inherits(self):
        l_fqtn = []
        for elt in self._cog_inherits:
            if elt is Table:
                continue
            if hasattr(elt, '_cog_schemaname'):
                if elt is not self.__class__:
                    fqtn = "%s.%s" % (elt._cog_schemaname, elt._cog_tablename)
                l_fqtn.append(fqtn)
        return l_fqtn

    @property
    def cog_table(self):
        namespace = self.db.table(
            'collorg.core.namespace', name_=self._cog_schemaname)
        table = namespace._rev_data_type_
        return table

    def has_field(self, fieldname):
        return fieldname in self.__dict__

    @property
    def retrieved(self):
        return self.__retrieved

    @property
    def uniq(self):
        return self.__uniq

    def _not_retrieved(self):
        self.__uniq = False
        self.__retrieved = False

    @property
    def schemaname(self):
        return self._cog_schemaname

    @property
    def name(self):
        return self._cog_tablename

    @property
    def neighbors(self):
        return self.__neighbors

    @property
    def rev_neighbors(self):
        return self.db.rev_neighbors(self.fqtn)

    def _is_linked_to(self, obj):
        return obj.fqtn in self.__neighbors

    def __call__(self, *args, **kwargs):
        return self.__class__(self.db, *args, **kwargs)

    # SQL substitutes

    def __get_inh_namespaces(self):
        namespaces = []
        for cls in inspect.getmro(self.__class__):
            namespace = None
            if '_cog_schemaname' in cls.__dict__:
                namespace = cls._cog_schemaname
                if not namespace or namespace in namespaces:
                    continue
                namespaces.append(namespace)
        return namespaces

    def _cog_SQL_insert(self, commit):
        """
        """
        cog_oid = None
        req = []
        from collorg.db.core.base_table import Base_table
        class_hierarchy = inspect.getmro(self.__class__)
        if Base_table in class_hierarchy:
            # we duplicate the information in "collorg.core.oid_table"
            cog_oid = uuid.uuid4()
            cog_fqtn = self.fqtn
            deja_vu_namespace = []
            for namespace in self.__get_inh_namespaces():
                req.append("""INSERT INTO "{}".oid_table VALUES """
                    """('{}', '{}')""".format(namespace, cog_oid, cog_fqtn))
            self.cog_oid_.value = cog_oid
            if ('cog_environment_' in self.__dict__ and
                self.cog_environment_.value is None):
                self.cog_environment_.value = cog_oid
        fields = []
        values = []
        for field in self._cog_fields:
            if field.is_constrained:
                fields.append('"{}"'.format(field.orig_name))
                qv = field.quoted_val()
                if type(qv) is list or type(qv) is tuple:
                    qv = "({})".format(self._cog_SQL_gen_request(qv))
                values.append(qv)
        req.append("INSERT INTO {} ({}) VALUES ({})".format(
            self.sql_fqtn, ", ".join(fields), ", ".join(values)))
        req = ";\n".join(req)
        return req, cog_oid

    def insert(self, just_return_sql=False, commit=True):
        if self.db.test_mode and self.has_field('cog_test_'):
            self.cog_test_.value = True
            self.cog_signature_.value = id(self.db)
        sql_req, cog_oid = self._cog_SQL_insert(commit=commit)
        if just_return_sql:
            return sql_req
        self.db.raw_sql(sql_req, ())
        if cog_oid:
            # FORCE DATA RELOAD !!!
            i = 0
            while i < 5:
                time.sleep(0.01)
                i += 1
                try:
                    #XXX If removed (try), the reply on a comment will fail!
                    this = self.db.table(self.fqtn)
                    this.cog_oid_.value = cog_oid
                    self = this.get()
                    break
                except:
                    pass
        return self

    def __sql_delete(self, no_clause):
        """
        TEST CONSTRUCTION REQUETE SELECT. SANS DOUTE PAS ICI!!
        """
        req = ["DELETE"]
        req.append("FROM\n%s" % (self._cog_SQL_from()))
        if self._cog_is_constrained():
            req.append(self._cog_SQL_gen_request(self._cog_SQL_where()))
        else:
            if not no_clause:
                raise Exception(
                    "Attempting to delete all tuples without no_clause!")
        return "\n".join(req)

    def _cog_SQL_delete(self, orig, just_return_sql, no_clause, commit):
        sql_req = []
        if self._cog_base_table:
            for namespace in self.__get_inh_namespaces():
                try:
                    oid = self.db.table('{}.oid_table'.format(namespace))
                except:
                    continue #XXX TO BE REMOVED
                oid.cog_oid_.value = self.cog_oid_
                sql_req.append(oid.delete(just_return_sql=True))
        if just_return_sql:
            no_clause = True
        sql_req.append(self.__sql_delete(no_clause=no_clause))
        if just_return_sql:
            return ";\n".join(sql_req)
        try:
            self.db.raw_sql(";\n".join(sql_req), ())
        except Exception as e:
            self.db.rollback()
            raise CustomError("Delete error:%s\n%s" % (e, sql_req))
        #!! à quoi bon retourner l'objet supprimé?
        return self

    def delete(self, just_return_sql=False, no_clause=False, commit=True):
        return self._cog_SQL_delete(
            orig=self, just_return_sql=just_return_sql,
            no_clause=no_clause, commit=commit)

    def __sql_update(self, new_, no_clause=False):
        assert self.__class__ is new_.__class__
        l_fields = []
        clause = False
        for field in new_._cog_fields:
            if field.is_constrained:
                clause = True
                l_fields.append(field)
        what = ", ".join(
            ['"%s" = %s' % (field.name, field.quoted_val()) for field in l_fields])
        self.__sql = "UPDATE %s SET %s" % (new_.sql_fqtn, what)
        l_fields = []
        for field in self._cog_fields:
            if field.is_constrained:
                clause = True
                l_fields.append(field)
        where_fields = "(\n %s)" % ", ".join(
            ['"{}"'.format(field.name) for field in l_fields])
        if clause:
            self.__sql += " WHERE %s IN (\n %s)" % (
                where_fields,
                self.select(fields=l_fields, just_return_sql=True))
        else:
            if not no_clause:
                raise CustomError("Attempting to delete all tuples from %s" % (
                        self.sql_fqtn))
        return self.__sql.strip()

    def _cog_SQL_update(
        self, update_tuple, just_return_sql, no_clause, commit,
        update_modif_date=True):
        """
        met à jour le self avec les valeurs du n_self
        """
        if just_return_sql:
            no_clause = True
        if self.has_field('cog_modif_date_') and update_modif_date:
            update_tuple.cog_modif_date_.value = datetime.now()
        sql_req = self.__sql_update(update_tuple, no_clause)
        if just_return_sql:
            return sql_req
        try:
            self.db.raw_sql(sql_req, ())
        except Exception as e:
            self.db.rollback()
            raise CustomError("Update error:%s\n%s" % (e, sql_req))
        ## màj. de update_tuple pour y intégrer les nouvelles valeurs
        for field in update_tuple._cog_fields:
            if field.is_constrained:
                self.__dict__["%s_" % (field.name)].value = field.value
        return self

    def update(
        self, update_tuple, just_return_sql=False, no_clause=False,
            update_modif_date=True, commit=True):
        return self._cog_SQL_update(
            update_tuple, just_return_sql=just_return_sql,
            no_clause=no_clause, update_modif_date=update_modif_date,
            commit=commit)

    # goodies

    def increment(self, field, value=1):
        assert float(value)
        req = []
        req.append('update {} set "{}" = "{}" + {}'.format(
            self._cog_SQL_from(), field.name, field.name, value))
        req.append(self._cog_SQL_gen_request(self._cog_SQL_where()))
        sql_req = "\n".join(req)
        self.db.raw_sql(sql_req, ())
        return self

    # REFACTORING

    @property
    def _cog_fields(self):
        """
        @return: An iterator over the fields of the self
        """
        for elt in self.__l_fields:
            field = self.__dict__[elt]
            if field.__class__ is Field:
                yield field

    def is_(self, other):
        """returns true if all fields are equal"""
        if self.__class__ != other.__class__:
            return False
        for field in self._cog_fields:
            if field.value != other.__dict__[field.pyname].value:
                return False
        return True

    def dup(self, *args):
        """
        Duplicates self with the fields passed by args.
        If no argument is passed, all the fields are duplicated
        """
        raise NotImplementedError
        dup_obj = self.db.table(self.fqtn)
        if len(args) == 0:
            args = self._cog_fields
        for field in args:
            dup_obj.__dict__[field.pyname].value = field
        return dup_obj

    def showstruct(self):
        res = ["        fields list:"]
        l_fields = self.db.metadata.fields_names(
            self._cog_schemaname, self._cog_tablename)
        for fieldname in l_fields:
            fref = self.db._metadata.\
                metadata[self._cog_schemaname]['d_tbl']\
                [self._cog_tablename]['d_fld'][fieldname]
            machin = []
            machin.append(fref['fieldtype'])
            fref["inherited"] and machin.append("inherited")
            fref['pkey'] and machin.append("PK")
            fref['uniq'] and machin.append("uniq")
            fref['notnull'] and machin.append("not null")
            fref['fkeyname'] and machin.append("FK")
            res_line = "        * %s_ : %s"
            res.append(res_line % (fieldname, ", ".join(machin)))
        return res

    # NOT HERE! TO BE REMOVED!
    def _w3debug(self, **kwargs):
        from collorg.templates.document_type.html import Html
        action_name = kwargs['func'].func_name
        data_type = self.fqtn
        duration = kwargs['duration']
        begin_funct  = kwargs['begin_funct']
        begin = kwargs['begin']
        action = self.db.relation('collorg.application.action')
        action.name_.value = action_name
        action.data_type_.value = data_type
        try:
            action.get()
            label = '<img src="/collorg/images/firebug-small.png" class="debug">'
            link = Html(action).a(cog_method="w3debug_action",
                label=label, data_type=data_type, debug=False)
        except:
            link = "oups"
        return """
        <div class="{}" title="module: {}
        method: {}
        prep: {}
        duration: {}">{}{}</div>
        """.format(
            kwargs['trace'], data_type,
            action_name,
            kwargs['begin_funct'] - kwargs['begin'],
            kwargs['duration'],
            kwargs['f_res'],
            link)
